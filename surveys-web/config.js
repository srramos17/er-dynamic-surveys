'use strict'

const config = {
  client : {
    endpoints: {
      users: 'http://api.cuestionarios.com/user'
    }
  },
  secret: process.env.CUESTIONARIOS_SECRET || 's3cr3t'
}

if (process.env.NODE_ENV !== 'production') {
  config.client.endpoints = {
    users: 'http://localhost:5001',
    surveys: 'http://localhost:5002',
    roles: 'http://localhost:5003',
    groups: 'http://localhost:5004',
    questions: 'http://localhost:5005'
  }
}

module.exports = config;
