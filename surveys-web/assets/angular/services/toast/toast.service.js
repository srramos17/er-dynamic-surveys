'use strict'

angular.module('Toast')
.service('toast', ['$mdToast', function($mdToast){
  this.SimpleToast = function (msg) {
        $mdToast.show(
            $mdToast.simple()
                .textContent(msg)
                .position('top')
                .hideDelay(5000)
        );
    };

    this.ActionToast = function (textAction, msg, funcAction) {
        var toast = $mdToast.simple()
            .textContent(msg)
            .action(textAction)
            .highlightAction(true)
            .highlightClass('md-accent')
            .position('top');

        return $mdToast.show(toast);
    };
    /*
    Ejemplo de uso del ActionToast:

    Toast.ActionToast('OK', 'Ha ocurrido un error al iniciar sesión').then(function (response) {
            if (response == 'ok') {//ok es el resultado de presionar el botón de acción. No tiene relacion con el texto mostrado.
                alert('Que hago ahora!!!');
            }
        });
    */

    this.CustomToast = function () {//Pendiente implementar la forma de pasar datos al controlador.
        $mdToast.show({
            hideDelay: 5000,
            position: 'top',
            template: '<toast></toast>'
        });
    };
}])