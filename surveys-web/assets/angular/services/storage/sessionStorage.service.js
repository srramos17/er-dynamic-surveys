'use strict'

angular.module('serviceStorage')
.service('SessionStorage', ['', () => {
  this.SetParam = (key, value) => {
    sessionStorage.removeItem(key);
    sessionStorage.setItem(key, JSON.stringify(value));
  }

  this.GetParam = (key) => {
    if (sessionStorage.getItem(key) !== undefined ||
    sessionStorage.getItem(key) !== null) {
      return JSON.parse(sessionStorage.getItem(key));
    }
  }

  this.ClearParam = (key) => {
    sessionStorage.removeItem(key);
  }
}])