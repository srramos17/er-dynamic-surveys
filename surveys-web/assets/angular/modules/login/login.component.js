'use strict'

angular.module('login')
.component('login', {
  templateUrl: './templates/login/login.template.html',
  controller: ['$http','FactoryUser', 'toast' , '$location', function($http, FactoryUser, toast, $location) {
    let ct = this
    ct.User = {}
    ct.MainUrl = ''//Constants.MainUrl
    ct.IniciandoSesion = false

    ct.SigIn = function () {
        const req = {
            method: 'POST',
            url: '/auth',
            data: { username: ct.User.UserName, password: ct.User.Password }
        }

        $http(req)
        .then((res) => {
            window.location.href = 'http://localhost:3000/cd'
        }, (err) => {
            if(err.status === 401){
                toast.SimpleToast('User or password invalid.')
            }
            else if(err.status === -1){
                toast.SimpleToast('El servicio de autenticación no está disponible')
            } else {
                console.log(err.status + ' - ' + err.statusText)
                toast.SimpleToast(err.status + ' - ' + err.statusText)
            }
        })
    }
  }]
})