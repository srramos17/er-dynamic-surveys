'use strict'

angular.module('ToastModule')
.component('toast',{
  templateUrl: './templates/toast/toast.template.html',
  controller: ['$mdToast', '$mdDialog', ($mdToast, $mdDialog) => {
    let isDlgOpen

    this.closeToast = () => {
        if (isDlgOpen) return

        $mdToast
            .hide()
            .then(() => {
                isDlgOpen = false
            })
    };

    this.openMoreInfo = (e) => {
        if (isDlgOpen) return
        isDlgOpen = true

        $mdDialog
            .show($mdDialog
                .alert()
                .title('More info goes here.')
                .textContent('Something witty.')
                .ariaLabel('More info')
                .ok('Got it')
                .targetEvent(e)
            )
            .then(() => {
                isDlgOpen = false;
            });
    };
  }]
})