'use strict'

angular.module('Home')
.component('home', {
  templateUrl: './templates/home/home.template.html',
  controller: function ()  {
    this.mensaje = 'Mensaje del controlador'
  }
})