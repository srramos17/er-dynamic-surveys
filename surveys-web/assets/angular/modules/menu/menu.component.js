'use strict'

angular.module('Menu')
.component('menu', {
  templateUrl: './templates/menu/menu.template.html',
  controller: ['$window', '$location', '$mdComponentRegistry', '$http', function($window, $location, $mdComponentRegistry, $http) {
    let originatorEv
    let ct = this
    ct.MainUrl = ''//Constants.MainUrl;
    ct.Pin = false
    ct.SideLocked = 'md-locked-open'
    ct.classLock = "lock_open"

    ct.Init = () => {
        alert('Iniciando')
    }

    ct.PinSideBar = () => {
        if (ct.Pin) {
            ct.Pin = false
            ct.classLock = "lock_open"
            $mdComponentRegistry.when('left').then((it) => {
                it.toggle()
            });
        }
        else {
            ct.Pin = true
            ct.classLock = "lock"
            ct.SideLocked = 'md-locked-open'
        }
    }

    ct.ToggleSide = () => {
        $mdComponentRegistry.when('left').then((it) => {
            it.toggle()
        });
    }

    ct.CloseSide = () => {
        $mdComponentRegistry.when('left').then((it) => {
            it.toggle()
        });
    }

    ct.LogOut = () => {
        const req = {
            method: 'GET',
            url: '/logout'
        }

        $http(req).then((res) => {
            if (res && res.status === 201) {
                window.location.href = 'http://localhost:3000/'
            }
        }, (err) => {
            console.log(err.data.message)
        })
    }

    ct.MiCuenta = () => {

    }

    ct.Cuestionarios = () => {

    }

    ct.Administracion = () => {

    }
  }]
})