'use strict'

angular.module('Users')
.component('users', {
  templateUrl: './templates/admin/users/users.template.html',
  controller: ['$scope','toast', '$http', '$mdDialog', '$timeout', function($scope, toast, $http, $mdDialog, $timeout){
    let ct = this
    ct.Users = []

    $scope.$watch(() => {
        $('.chRepeat > div.md-label').remove()
    })

    ct.Init = () => {
        ct.Users = getUsers()
    }

    ct.deleteSelectedRows = (r, ev) => {
        const temp = ct.Users
        ct.Users = []
        var confirm = $mdDialog.confirm()
              .title('Desea continuar ?')
              .textContent('Se eliminarán los usuarios seleccionados. No se podrá deshacer.')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('Si deseo!')
              .cancel('Olvídalo');

        $mdDialog.show(confirm).then(function() {
         dropUser(r)
        }, () => {
            ct.Users = temp
        });
    }

    const getUsers = () => {
        $http.get('/getUsers')
        .then((res) => {
            ct.Users = res.data
        }, (err) => {
            toast.SimpleToast('No fue posible obtener los usuarios.')
        })
    }

    const dropUser = (r) => {
        const req = {
            method: 'POST',
            url: '/dropUser',
            data: r
        }
        $http(req).then((res) => {
            getUsers()
        }, (err) => {
            toast.SimpleToast('No fue posible eliminar los usuarios.')
        })
    }
  }]
})