'use strict'

angular.module('Users')
.component('usersDetail', {
  templateUrl: './templates/admin/users/users.detail.template.html',
  controller: ['$routeParams', 'toast', '$http', '$timeout', '$location', function($routeParams, toast, $http, $timeout, $location) {
    let ct = this
    ct.TextoTitulo = "Crear"
    ct.TextoBotonAccion = "Crear"
    ct.idUser = 0
    ct.documentTypes = {}
    ct.Roles = {}
    ct.Companies = {}
    ct.Branches = {}

    ct.Init = function () {
        ct.idUser = $routeParams.id

        if (ct.idUser != 0) {
            ct.TextoTitulo = "Editar"
            ct.TextoBotonAccion = "Guardar"

            ct.getUser(ct.idUser)
        } else {
            ct.getDocumentTypes()
        }
    }

    ct.getUser = (id) => {
        $http.get(`/user/${id}`)
        .then((res) => {
            ct.User = res.data[0]
        }, (err) => {
            console.log(err.data.message)
            toast.SimpleToast('Cannot get user data')
        }).then(() => ct.getRoles())
    }

    ct.getDocumentTypes = () => {
        $http.get(`/getDocuments`)
        .then((res) => {
            ct.documentTypes = res.data
        }, (err) => {
            console.log(err.data.message)
        }).then(() => ct.getRoles())
    }

    ct.getRoles = () => {
        $http.get(`/getRoles`)
        .then((res) => {
            ct.Roles = res.data.filter((rol) => rol.active === true)
        }, (err) => {
            console.log(err.data.message)
        }).then(() => ct.getCompanies())
    }

    ct.getCompanies = () => {
        $http.get(`/getCompanies`)
        .then((res) => {
            ct.Companies = res.data.filter((com) => com.active === true)
        }, (err) => {
            console.log(err.data.message)
        }).then(() => { if (ct.User) {ct.getBranches()}})
    }

    ct.getBranches = () => {
        let idCompany = ct.User.companyId
        $http.get(`/getBranchesByCompany/${idCompany}`)
        .then((res) => {
            ct.Branches = res.data.filter((br) => br.active === true)
        }, (err) => {
            console.log(err.data.message)
        })
    }

    ct.GoBack = function () {
        window.history.back()
    }

    ct.AddOrEditUser = function () {
        const req = {
            method: 'POST',
            url: '/user',
            data: ct.User
        }

        if (ct.idUser != 0) {
            req.url = '/updateUser'
        }

        $http(req).then((res) => {
            if(res.status === 200) {
                if (ct.idUser != 0) {
                    toast.SimpleToast('user updated')
                } else {
                    toast.SimpleToast('user created')
                }

                $timeout(() => {
                    $location.url('/cd/admin/usuarios')
                }, 3000)
            }
        }, (err) => {
            toast.SimpleToast('An error has ocurred. Retry again')
        })
    }
  }]
})