'use strict'

angular.module('Questions')
.component('questions', {
  templateUrl: './templates/admin/questions/questions.template.html',
  controller: ['$scope','toast', '$http', '$mdDialog', '$timeout', function($scope, toast, $http, $mdDialog, $timeout){
    let ct = this
    ct.Questions = []

    $scope.$watch(function () {
        $('.chRepeat > div.md-label').remove()
    })

    ct.Init = () => {
        ct.Questions = getQuestions()
    }

    ct.deleteSelectedRows = (r, ev) => {
        const temp = ct.Questions
        ct.Questions = []
        var confirm = $mdDialog.confirm()
              .title('Desea continuar ?')
              .textContent('Se eliminarán las preguntas seleccionadas. No se podrá deshacer.')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('Si deseo!')
              .cancel('Olvídalo');

        $mdDialog.show(confirm).then(function() {
         dropQuestion(r)
        }, () => {
            ct.Questions = temp
        });
    }

    const getQuestions = () => {
        $http.get('/getQuestions')
        .then((res) => {
            ct.Questions = res.data
        }, (err) => {
            toast.SimpleToast('No fue posible obtener las preguntas.')
        })
    }

    const dropQuestion = (r) => {
        const req = {
            method: 'POST',
            url: '/dropQuestion',
            data: r
        }

        $http(req).then((res) => {
            getQuestions()
        }, (err) => {
            toast.SimpleToast('No fue posible eliminar la(s) pregunta(s).')
        })
    }
  }]
})