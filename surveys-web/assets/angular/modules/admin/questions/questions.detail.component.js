'use strict'

angular.module('Questions')
    .component('questionsDetail', {
        templateUrl: './templates/admin/questions/questions.detail.template.html',
        controller: ['$routeParams', 'toast', '$http', '$timeout', '$location', function ($routeParams, toast, $http, $timeout, $location) {
            let ct = this
            ct.TextoTitulo = "Crear"
            ct.TextoBotonAccion = "Crear"
            ct.idPregunta = 0
            ct.Pregunta = {}
            ct.dicotomica = []
            ct.politomica = []

            ct.Init = function () {
                ct.idPregunta = $routeParams.id

                if (ct.idPregunta != 0) {
                    ct.TextoTitulo = "Editar"
                    ct.TextoBotonAccion = "Guardar"
                    ct.getQuestion(ct.idPregunta)
                }
            }

            ct.getQuestion = (id) => {
                $http.get(`/question/${id}`)
                    .then((res) => {
                        ct.Pregunta = res.data[0]
                        setData(ct.Pregunta)
                    }, (err) => {
                        console.log(err.data.message)
                        toast.SimpleToast('Cannot get question data')
                    })
            }

            ct.GoBack = function () {
                window.history.back()
            }

            ct.AddOrEditQuestion = function () {
                validateData()

                const req = {
                    method: 'POST',
                    url: '/question',
                    data: ct.Pregunta
                }

                if (ct.idPregunta != 0) {
                    req.url = '/updateQuestion'
                }

                $http(req).then((res) => {
                    if (res.status === 200) {
                        if (ct.idPregunta != 0) {
                            toast.SimpleToast('Question updated')
                        } else {
                            toast.SimpleToast('Question created')
                        }

                        $timeout(() => {
                            $location.url('/cd/admin/preguntas')
                        }, 3000)
                    }
                }, (err) => {
                    toast.SimpleToast('An error has ocurred. Retry again')
                })
            }

            const validateData = () => {
                switch (ct.typeResponse) {
                    case "Dicotómica":
                        ct.Pregunta.responseType = 0
                        ct.Pregunta.dichotomous = ct.dicotomica
                        break;
                    case "Politómica":
                        ct.Pregunta.responseType = 1
                        ct.Pregunta.polythomous = ct.politomica
                        break;
                    case "Numérica":
                        ct.Pregunta.responseType = 2
                        ct.Pregunta.numeric = ct.numeric
                        break;
                    case "Alfanumérica":
                        ct.Pregunta.responseType = 3
                        ct.Pregunta.alphanumerical = ct.alphanumerical
                        break;
                    case "Fecha":
                        ct.Pregunta.responseType = 4
                        ct.Pregunta.date = ct.date
                        break;
                    case "Multilinea":
                        ct.Pregunta.responseType = 5
                        ct.Pregunta.multiline = ct.multiline
                        break;
                }
            }

            const setData = (dt) => {
                switch (dt.responseType) {
                    case 0:
                        ct.typeResponse = "Dicotómica"
                        ct.dicotomica = ct.Pregunta.dichotomous
                        break;
                    case 1:
                        ct.typeResponse = "Politómica"
                        ct.politomica = ct.Pregunta.polythomous
                        break;
                    case 2:
                        ct.typeResponse = "Numérica"
                        ct.numeric = ct.Pregunta.numeric
                        break;
                    case 3:
                        ct.typeResponse = "Alfanumérica"
                        ct.alphanumerical = ct.Pregunta.alphanumerical
                        break;
                    case 4:
                        ct.typeResponse = "Fecha"
                        ct.date = ct.Pregunta.date
                        break;
                    case 5:
                        ct.typeResponse = "Multilinea"
                        ct.multiline = ct.Pregunta.multiline
                        break;
                }
            }
        }]
    })