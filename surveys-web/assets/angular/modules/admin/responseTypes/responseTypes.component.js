'use strict'

angular.module('ResponseTypes')
.component('responseTypes', {
  templateUrl: './templates/admin/responseTypes/responseTypes.template.html',
  controller: ['$scope', 'toast', function($scope,toast){
    let ct = this
    ct.ResponseTypes = []

    $scope.$watch(function () {
        $('.chRepeat > div.md-label').remove()
    })

    ct.Init = function () {
        //TODO: Validar session del usuario
        ct.ResponseTypes = getResponseTypes()
    }

    ct.deleteSelectedRows = function () {
        toast.SimpleToast('Eliminado')
    }

    ct.GoBack = function () {
        window.history.back()
    }

    function getResponseTypes() {
        return [
            {
                Id: 1,
                ResponseType: 'Politomica',
                Activo: true
            },
            {
                Id: 2,
                ResponseType: 'Dicotomica',
                Activo: false
            },
            {
                Id: 3,
                ResponseType: 'Fecha',
                Activo: true
            }
        ]
    }
  }]
})