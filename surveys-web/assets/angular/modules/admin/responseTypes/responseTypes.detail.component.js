'use strict'

angular.module('ResponseTypes')
.component('responseTypesDetail', {
  templateUrl: './templates/admin/responseTypes/responseTypes.detail.template.html',
  controller: ['$routeParams', 'toast', function($routeParams, toast){
    let ct = this
    ct.TextoTitulo = "Crear"
    ct.TextoBotonAccion = "Crear"
    ct.idResponseType = 0
    ct.ResponseType = {}

    ct.Init = function () {
        //TODO: Validar session del usuario
        ct.idResponseType = $routeParams.id

        if (ct.idResponseType > 0) {
            ct.TextoTitulo = "Editar"
            ct.TextoBotonAccion = "Guardar"
            //TODO: Cargar ResponseType por el id.
        }
    }

    ct.GoBack = function () {
        window.history.back()
    }

    ct.AddOrEditResponseType = function () {
        if (ct.idResponseType !== undefined) {
            if (ct.idResponseType > 0) {
                //TODO: Actualizar ResponseType
                toast.SimpleToast('ResponseType updated')
            }
            else {
                //TODO: Crear ResponseType
                toast.SimpleToast('ResponseType created')
                //TODO: Consultar de nuevo los ResponseTypes para refrescar la lista.
            }
        }
    }
  }]
})