'use strict'

angular.module('Polles')
.component('polles', {
  templateUrl: './templates/admin/polles/polles.template.html',
  controller: ['$scope','toast', '$http', '$mdDialog', '$timeout', function($scope, toast, $http, $mdDialog, $timeout){
    let ct = this
    ct.Polles = []

    $scope.$watch(() => {
        $('.chRepeat > div.md-label').remove()
    })

    ct.Init = () => {
        ct.Polles = getPolles()
    }

    ct.deleteSelectedRows = (r, ev) => {
        const temp = ct.Polles
        ct.Polles = []
        var confirm = $mdDialog.confirm()
              .title('Desea continuar ?')
              .textContent('Se eliminarán los cuestionarios seleccionados. No se podrá deshacer.')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('Si deseo!')
              .cancel('Olvídalo');

        $mdDialog.show(confirm).then(function() {
         dropPoll(r)
        }, () => {
            ct.Polles = temp
        });
    }

    const getPolles = () => {
        $http.get('/getPolles')
        .then((res) => {
            ct.Polles = res.data
        }, (err) => {
            toast.SimpleToast('No fue posible obtener los cuestionarios.')
        })
    }

    const dropPoll = (r) => {
        const req = {
            method: 'POST',
            url: '/dropPoll',
            data: r
        }
        $http(req).then((res) => {
            getPolles()
        }, (err) => {
            toast.SimpleToast('No fue posible eliminar los cuestionarios.')
        })
    }
  }]
})