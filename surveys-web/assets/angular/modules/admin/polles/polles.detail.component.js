'use strict'

angular.module('Polles')
.component('pollesDetail' ,{
  templateUrl: './templates/admin/polles/polles.detail.template.html',
  controller: ['$routeParams', 'toast', '$http', '$timeout', '$location', function($routeParams, toast, $http, $timeout, $location) {
    let ct = this
    ct.TextoTitulo = "Crear"
    ct.TextoBotonAccion = "Crear"
    ct.idCuestionario = 0

    ct.Init = function () {
        ct.idCuestionario = $routeParams.id

        if (ct.idCuestionario != 0) {
            ct.TextoTitulo = "Editar"
            ct.TextoBotonAccion = "Guardar"

            ct.getPoll(ct.idCuestionario)
        }
    }

    ct.getPoll = (id) => {
        $http.get(`/poll/${id}`)
        .then((res) => {
            ct.Poll = res.data
        }, (err) => {
            console.log(err.data.message)
            toast.SimpleToast('Cannot get poll data')
        })
    }

    ct.GoBack = function () {
        window.history.back()
    }

    ct.AddOrEditCuestionario = function () {
        const req = {
            method: 'POST',
            url: '/poll',
            data: ct.Poll
        }

        if (ct.idCuestionario != 0) {
            req.url = '/updatePoll'
        }

        $http(req).then((res) => {
            if(res.status === 200) {
                if (ct.idCuestionario != 0) {
                    toast.SimpleToast('poll updated')
                } else {
                    toast.SimpleToast('poll created')
                }

                $timeout(() => {
                    $location.url('/cd/admin/cuestionarios')
                }, 3000)
            }
        }, (err) => {
            toast.SimpleToast('An error has ocurred. Retry again')
        })
    }
  }]
})