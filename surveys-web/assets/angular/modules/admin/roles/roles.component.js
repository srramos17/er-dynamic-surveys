'use strict'

angular.module('Roles')
.component('roles', {
  templateUrl: './templates/admin/roles/roles.template.html',
  controller: ['$scope', '$mdDialog', '$http', 'toast', function($scope, $mdDialog, $http, toast){
    let ct = this
    ct.Roles = [];

    $scope.$watch(function () {
        $('.chRepeat > div.md-label').remove();
    });

    ct.Init = function () {
        ct.Roles = getRoles();
    }

    ct.deleteSelectedRows = (r, ev) => {
        const temp = ct.Roles
        ct.Roles = []
        var confirm = $mdDialog.confirm()
              .title('Desea continuar ?')
              .textContent('Se eliminarán los roles seleccionados. No se podrá deshacer.')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('Si deseo!')
              .cancel('Olvídalo');

        $mdDialog.show(confirm).then(function() {
         dropRole(r)
        }, () => {
            ct.Roles = temp
        });
    }

    const getRoles = () => {
        $http.get('/getRoles')
        .then((res) => {
            ct.Roles = res.data
        }, (err) => {
            toast.SimpleToast('No fue posible obtener los roles.')
        })
    }

    const dropRole = (r) => {
        const req = {
            method: 'POST',
            url: '/dropRole',
            data: r
        }
        $http(req).then((res) => {
            getRoles()
        }, (err) => {
            toast.SimpleToast('No fue posible eliminar los roles.')
        })
    }
  }]
})