'use strict'

angular.module('Roles')
.component('rolesDetail', {
  templateUrl: './templates/admin/roles/roles.detail.template.html',
  controller: ['$routeParams', 'toast', '$http', '$timeout', '$location', function($routeParams, toast, $http, $timeout, $location) {
    let ct = this
    ct.TextoTitulo = "Crear";
    ct.TextoBotonAccion = "Crear";
    ct.idUsuario = 0;
    ct.Role = {};

    ct.Init = function () {
        ct.idUsuario = $routeParams.id

        if (ct.idUsuario != 0) {
            ct.TextoTitulo = "Editar";
            ct.TextoBotonAccion = "Guardar";
            ct.getRole(ct.idUsuario)
        }
    }

    ct.getRole = (id) => {
        $http.get(`/role/${id}`)
        .then((res) => {
            ct.Role = res.data
        }, (err) => {
            console.log(err.data.message)
            toast.SimpleToast('Cannot get role data')
        })
    }

    ct.GoBack = function () {
        window.history.back()
    }

    ct.AddOrEditRole = function () {
        const req = {
            method: 'POST',
            url: '/role',
            data: ct.Role
        }

        if (ct.idUsuario != 0) {
            req.url = '/updateRole'
        }

        $http(req).then((res) => {
            if(res.status === 200) {
                if (ct.idUsuario != 0) {
                    toast.SimpleToast('role updated')
                } else {
                    toast.SimpleToast('role created')
                }

                $timeout(() => {
                    $location.url('/cd/admin/perfiles')
                }, 3000)
            }
        }, (err) => {
            toast.SimpleToast('An error has ocurred. Retry again')
        })
    }
  }]
})