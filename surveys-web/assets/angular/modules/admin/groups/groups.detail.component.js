'use strict'

angular.module('Groups')
.component('groupsDetail', {
  templateUrl: './templates/admin/groups/groups.detail.template.html',
  controller: ['$routeParams', 'toast', '$http', '$timeout', '$location', function($routeParams, toast, $http, $timeout, $location) {
    let ct = this
    ct.TextoTitulo = "Crear"
    ct.TextoBotonAccion = "Crear"
    ct.idGrupo = 0

    ct.Init = function () {
        ct.idGrupo = $routeParams.id

        if (ct.idGrupo != 0) {
            ct.TextoTitulo = "Editar"
            ct.TextoBotonAccion = "Guardar"
            ct.getGroup(ct.idGrupo)
        }
    }

    ct.getGroup = (id) => {
        $http.get(`/group/${id}`)
        .then((res) => {
            ct.Group = res.data
        }, (err) => {
            console.log(err.data.message)
            toast.SimpleToast('Cannot get group data')
        })
    }

    ct.GoBack = function () {
        window.history.back()
    }

    ct.AddOrEditGroup = function () {
        const req = {
            method: 'POST',
            url: '/group',
            data: ct.Group
        }

        if (ct.idGrupo != 0) {
            req.url = '/updateGroup'
        }

        $http(req).then((res) => {
            if(res.status === 200) {
                if (ct.idGrupo != 0) {
                    toast.SimpleToast('group updated')
                } else {
                    toast.SimpleToast('group created')
                }

                $timeout(() => {
                    $location.url('/cd/admin/grupos')
                }, 3000)
            }
        }, (err) => {
            toast.SimpleToast('An error has ocurred. Retry again')
        })
    }
  }]
})