'use strict'

angular.module('Groups')
.component('groups', {
  templateUrl: './templates/admin/groups/groups.template.html',
  controller: ['$scope','toast', '$http', '$mdDialog', '$timeout', function($scope, toast, $http, $mdDialog, $timeout){
    let ct = this
    ct.Groups = []

    $scope.$watch(function () {
        $('.chRepeat > div.md-label').remove()
    })

    ct.Init = () => {
        ct.Groups = getGroups()
    }

    ct.deleteSelectedRows = (r, ev) => {
        const temp = ct.Groups
        ct.Groups = []
        var confirm = $mdDialog.confirm()
              .title('Desea continuar ?')
              .textContent('Se eliminarán los grupos seleccionados. No se podrá deshacer.')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('Si deseo!')
              .cancel('Olvídalo');

        $mdDialog.show(confirm).then(function() {
         dropGroup(r)
        }, () => {
            ct.Groups = temp
        });
    }

    const getGroups = () => {
        $http.get('/getGroups')
        .then((res) => {
            ct.Groups = res.data
        }, (err) => {
            toast.SimpleToast('No fue posible obtener los grupos.')
        })
    }

    const dropGroup = (r) => {
        const req = {
            method: 'POST',
            url: '/dropGroup',
            data: r
        }
        $http(req).then((res) => {
            getGroups()
        }, (err) => {
            toast.SimpleToast('No fue posible eliminar los grupos.')
        })
    }
  }]
})