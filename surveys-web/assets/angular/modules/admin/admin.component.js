'use strict'

angular.module('Admin')
.component('admin', {
  templateUrl: './templates/admin/admin.template.html',
  controller: ['$location', function($location){
    let ct = this
    ct.Modulos = [];

    ct.Init = function () {
        //ct.CargarModulos();
        ct.Modulos = getModulos();
    }

    ct.CargarModulos = function () {
        //if (ct.form.$invalid) {
        //    return;
        //}


        //var url = "/Account/SigIn"
        // ServiceFactory(ct.User, url).then(function (response) {
        //     if (response.data.Result) {

        //     }

        // }).catch(function (err) {
        //     console.log(err);
        // });
    }

    ct.CargarModulo = function (url) {
        if (url) {
            $location.url(url);
        }
    }

    function getModulos() {
        return [
            {
                nombre: 'Usuarios',
                icon: './images/icons/usuarios.svg',
                class: 'darkBlue',
                alt: 'Usuarios',
                url: '/cd/admin/usuarios'
            },
            {
                nombre: 'Pefiles',
                icon: './images/icons/perfiles.svg',
                class: 'yellow',
                alt: 'Perfiles',
                url: '/cd/admin/perfiles'
            },
            {
                nombre: 'Cuestionarios',
                icon: './images/icons/cuestionarios.svg',
                class: 'purple',
                alt: 'Cuestionarios',
                url: '/cd/admin/cuestionarios'
            },
            {
                nombre: 'Grupos',
                icon: './images/icons/grupos.svg',
                class: 'blue',
                alt: 'Grupos',
                url: '/cd/admin/grupos'
            },
            {
                nombre: 'Preguntas',
                icon: './images/icons/preguntas.svg',
                class: 'red',
                alt: 'Preguntas',
                url: '/cd/admin/preguntas'
            },
            {
                nombre: 'Preguntas por Grupo',
                icon: './images/icons/tiposRespuestaa.svg',
                class: 'green',
                alt: 'Preguntas por Grupo',
                url: '/cd/admin/preguntasbygrupo'
            },
            {
                nombre: 'Grupos por Cuestionario',
                icon: './images/icons/tiposRespuestaa.svg',
                class: 'lightPurple',
                alt: 'Grupos por Cuestionario',
                url: '/cd/admin/gruposbycuestionario'
            },
        ];
    }
  }]
})