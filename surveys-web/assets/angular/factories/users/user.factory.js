'use strict'

angular.module('factory.user')
.factory('FactoryUser', ['$resource',
  function($resource) {

    function AuthUser (data) {
      return $resource('/auth')
    }

    return {
      AuthUser
    }
  }])