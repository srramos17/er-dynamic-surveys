﻿var ServiceFactory = function ($http) {
    return function (entity, serviceUrl) {

        return new Promise(function (resolve, reject) {
            $http.post(serviceUrl, entity).then(resolve, reject);
        });
    }
}

ServiceFactory.$inject = ["$http"];