﻿let constants = constants || {};
constants.StringEmpty = "";
constants.UrlRest = "Url Servicio REST. Solo si aplica";
constants.MainUrl = "http://localhost:3000/";

angular.module('MainApp',
    [
        'ngSanitize'
        , 'ngAnimate'
        , 'ngMessages'
        //, 'ui.router'
        , 'ngRoute'
        , 'ngMaterial'
        , 'mdDataTable'
        , 'ngResource'
        , 'angularUtils.directives.dirPagination'
        , 'lr.upload'
        , 'Toast'
        , 'serviceStorage'
        , 'Home'
        , 'Menu'
        , 'Admin'
        , 'Users'
        , 'Roles'
        , 'Polles'
        , 'Groups'
        , 'Questions'
        , 'ResponseTypes'
    ])
.constant('Constants', constants)
.config(['$locationProvider', '$routeProvider'
    , ($locationProvider, $routeProvider) => {
        $routeProvider
        .when('/cd', {
            template: '<home></home>'
        })
        .when('/cd/cuestionarios', {
            template: '<p>Bienvenido a custionarios</p>'
        })
        .when('/cd/reportes', {
            template: '<p>Bienvenido a reportes.</p>'
        })
        .when('/cd/admin', {
            template: '<admin></admin>'
        })
        .when('/cd/admin/usuarios', {
            template: '<users></users>'
        })
        .when('/cd/admin/usuarios/:id', {
            template: '<users-detail></users-detail>'
        })
        .when('/cd/admin/perfiles', {
            template: '<roles></roles>'
        })
        .when('/cd/admin/perfiles/:id', {
            template: '<roles-detail></roles-detail>'
        })
        .when('/cd/admin/cuestionarios', {
            template: '<polles></polles>'
        })
        .when('/cd/admin/cuestionarios/:id', {
            template: '<polles-detail></polles-detail>'
        })
        .when('/cd/admin/grupos', {
            template: '<groups></groups>'
        })
        .when('/cd/admin/grupos/:id', {
            template: '<groups-detail></groups-detail>'
        })
        .when('/cd/admin/preguntas', {
            template: '<questions></questions>'
        })
        .when('/cd/admin/preguntas/:id', {
            template: '<questions-detail></questions-detail>'
        })
        .when('/cd/admin/tiposRespuesta', {
            template: '<response-types></response-types>'
        })
        .when('/cd/admin/tiposRespuesta/:id', {
            template: '<response-types-detail></response-types-detail>'
        })
        .when('/cd/admin/preguntasbygrupo', {
            template: '<p>Preguntas por grupo</p>'
        })
        .when('/cd/admin/gruposbycuestionario', {
            template: '<p>Grupos por cuestionario</p>'
        })
        .otherwise({
        redirectTo: '/cd'
        })

        $locationProvider.html5Mode(true)
    }])

.service('ColorUtils', () => {

    this.GetRandomColor = () => {
        let color = [this.GetRandomInt(0, 255), this.GetRandomInt(0, 255), this.GetRandomInt(0, 255)];
        return this.GetRgb(color);
    }

    this.GetRandomInt = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    this.GetRgb = (arrayInt) => {
        return 'rgb(' + arrayInt.join(',') + ')';
    }
})

.factory("ServiceFactory", ServiceFactory);