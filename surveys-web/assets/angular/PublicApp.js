﻿let constants = constants || {}
constants.StringEmpty = ""
constants.UrlRest = "Url Servicio REST. Solo si aplica"
constants.MainUrl = "http://192.168.0.4:3000/"

angular.module('PublicApp',
    [
        'ngRoute'
        , 'ngMessages'
        , 'ngMaterial'
        , 'ngAnimate'
        , 'ngResource'
        , 'factory.user'
        , 'login'
        , 'Toast'
        , 'serviceStorage'
    ])

.constant('Constants', constants)
.config(['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) => {
    $routeProvider.
    when('/', {
      template: '<login></login>'
    })
    .when('/login', {
      template: '<login></login>'
    })
    .otherwise({
      redirectTo: '/'
    })

    $locationProvider.html5Mode(true);
}])
.factory("ServiceFactory", ServiceFactory);