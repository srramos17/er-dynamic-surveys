'use strict'

const LocalStrategy = require('passport-local').Strategy
const cuestionariosApp = require('surveys-client')
const config = require('../config')

let client = cuestionariosApp.CreateClient(config.client)

exports.localStrategy = new LocalStrategy((username, password, done) => {
  const credentials = {
    username: username,
    password: password
  }

  client.authUser(credentials, (err, token) => {
    if (err) {
      return done(err, false, {message: 'username and password not found'})
    }

    client.getUser(username, token, (err, user) => {
      if (err) {
        return done(err)
      }

      user.token = token
      return done(null, user)
    })
  })
})

exports.serializeUser = (user, done) => {
  done(null, {
    username: user.username,
    token: user.token
  })
}

exports.deserializeUser = (user, done) => {
  client.getUser(user.username, user.token, (err, usr) => {
    if (usr) {
      usr.token = user.token
    }
    done(err, usr)
  })
}