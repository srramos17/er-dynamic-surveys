const gulp = require('gulp')
const uglify = require('gulp-uglify')
const rename = require('gulp-rename')
const concat = require('gulp-concat')
const csso = require('gulp-csso')
const pump = require('pump')
const babel = require('gulp-babel')
const webpack = require('gulp-webpack')

gulp.task('css', (callback) => {
  pump([
    gulp.src(['./assets/css/**/*.css'])
    ,concat('styles.css')
    ,csso()
    ,gulp.dest('./public')
  ], callback)
})

gulp.task('html', (callback) => {
  pump([
    gulp.src('./views/**')
    ,gulp.dest('./public/views')
  ], callback)
})

gulp.task('manifest', (callback) => {
  pump([
    gulp.src('./manifest.json')
    ,gulp.dest('./public')
  ], callback)
})

gulp.task('images', (callback) => {
  pump([
    gulp.src('./assets/img/**')
    ,gulp.dest('./public/images')
  ], callback)
})

gulp.task('libreriasPublic',['angularTemplates', 'angularServices', 'angularFactories', 'angularModules'], (callback) => {
  pump([
    gulp.src([
      './assets/libraries/jquery-3.2.0.min.js'
      ,'./assets/libraries/lodash.min.js'
      ,'./assets/libraries/angular.min.js'
      ,'./assets/libraries/angular/angular-animate.min.js'
      ,'./assets/libraries/angular/angular-aria.min.js'
      ,'./assets/libraries/angular/angular-messages.min.js'
      ,'./assets/libraries/angular/angular-material.min.js'
      ,'./assets/libraries/angular/angular-route.min.js'
      ,'./assets/libraries/angular/angular-resource.min.js'
      , './assets/angular/ServiceFactory.js'
      , './assets/angular/PublicApp.js'])
    ,babel({presets: ['es2015']})
    ,concat('libsp.js')
    ,gulp.dest('./public')
  ], callback)
})

gulp.task('librerias', (callback) => {
  pump([
    gulp.src([
      './assets/libraries/jquery-3.2.0.min.js'
      ,'./assets/libraries/lodash.min.js'
      ,'./assets/libraries/angular.min.js'
      ,'./assets/libraries/angular/*.js'])
    ,babel({presets: ['es2015']})
    ,concat('libs.js')
    ,gulp.dest('./public')
  ], callback)
})

gulp.task('localAngular', (callback) => {
  pump([
    gulp.src([
      , './assets/angular/ServiceFactory.js'
      , './assets/angular/MainApp.js'])
    ,babel({presets: ['es2015']})
    ,concat('libsAngular.js')
    ,gulp.dest('./public')
  ], callback)
})

gulp.task('loginModule', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/login/login.module.js'
      ,'./assets/angular/modules/login/login.component.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('loginComponent.js')
    ,gulp.dest('./public/components')
  ], callback)
})

gulp.task('homeModule', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/home/home.module.js'
      ,'./assets/angular/modules/home/home.component.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('homeComponent.js')
    ,gulp.dest('./public/components')
  ], callback)
})

gulp.task('menuModule', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/menu/menu.module.js'
      ,'./assets/angular/modules/menu/menu.component.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('menuComponent.js')
    ,gulp.dest('./public/components')
  ], callback)
})

gulp.task('adminModule', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/admin/admin.module.js'
      ,'./assets/angular/modules/admin/admin.component.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('adminComponent.js')
    ,gulp.dest('./public/components')
  ], callback)
})

gulp.task('usersModule', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/admin/users/users.module.js'
      ,'./assets/angular/modules/admin/users/users.component.js'
      ,'./assets/angular/modules/admin/users/users.detail.component.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('usersComponent.js')
    ,gulp.dest('./public/components')
  ], callback)
})

gulp.task('rolesModule', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/admin/roles/roles.module.js'
      ,'./assets/angular/modules/admin/roles/roles.component.js'
      ,'./assets/angular/modules/admin/roles/roles.detail.component.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('rolesComponent.js')
    ,gulp.dest('./public/components')
  ], callback)
})

gulp.task('pollesModule', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/admin/polles/polles.module.js'
      ,'./assets/angular/modules/admin/polles/polles.component.js'
      ,'./assets/angular/modules/admin/polles/polles.detail.component.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('pollesComponent.js')
    ,gulp.dest('./public/components')
  ], callback)
})

gulp.task('groupsModule', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/admin/groups/groups.module.js'
      ,'./assets/angular/modules/admin/groups/groups.component.js'
      ,'./assets/angular/modules/admin/groups/groups.detail.component.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('groupsComponent.js')
    ,gulp.dest('./public/components')
  ], callback)
})

gulp.task('questionsModule', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/admin/questions/questions.module.js'
      ,'./assets/angular/modules/admin/questions/questions.component.js'
      ,'./assets/angular/modules/admin/questions/questions.detail.component.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('questionsComponent.js')
    ,gulp.dest('./public/components')
  ], callback)
})

gulp.task('responseTypesModule', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/admin/responseTypes/responseTypes.module.js'
      ,'./assets/angular/modules/admin/responseTypes/responseTypes.component.js'
      ,'./assets/angular/modules/admin/responseTypes/responseTypes.detail.component.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('responseTypesComponent.js')
    ,gulp.dest('./public/components')
  ], callback)
})

gulp.task('toastModule', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/toast/toast.module.js'
      ,'./assets/angular/modules/toast/toast.component.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('toastComponent.js')
    ,gulp.dest('./public/components')
  ], callback)
})

gulp.task('toastService', (callback) => {
  pump([
    gulp.src([
      './assets/angular/services/toast/toast.module.js'
      ,'./assets/angular/services/toast/toast.service.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('toastService.js')
    ,gulp.dest('./public/services')
  ], callback)
})

gulp.task('storageService', (callback) => {
  pump([
    gulp.src([
      './assets/angular/services/storage/storage.module.js'
      ,'./assets/angular/services/storage/sessionStorage.service.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('storageService.js')
    ,gulp.dest('./public/services')
  ], callback)
})

gulp.task('FactoryUser', (callback) => {
  pump([
    gulp.src([
      './assets/angular/factories/users/user.factory.module.js'
      ,'./assets/angular/factories/users/user.factory.js'])
    ,babel({presets: ['es2015']})
    ,uglify()
    ,concat('factoryUser.js')
    ,gulp.dest('./public/factories')
  ], callback)
})

gulp.task('templates', (callback) => {
  pump([
    gulp.src([
      './assets/angular/modules/**/**.html'])
    ,gulp.dest('./public/templates')
  ], callback)
})

gulp.task('scripts', ['libreriasPublic', 'librerias', 'localAngular'])
gulp.task('styles', ['css', 'images'])
gulp.task('views', ['manifest', 'html'])
gulp.task('angularModules', ['loginModule', 'toastModule', 'homeModule', 'menuModule', 'adminModule', 'usersModule', 'rolesModule', 'pollesModule', 'groupsModule', 'questionsModule', 'responseTypesModule'])
gulp.task('angularServices', ['toastService', 'storageService'])
gulp.task('angularFactories', ['FactoryUser'])
gulp.task('angularTemplates', ['templates'])
gulp.task('angular', ['angularModules', 'angularTemplates', 'angularServices', 'angularFactories'])
gulp.task('default', ['views', 'styles', 'scripts'])
gulp.task('watch', () => {
  gulp.watch('./assets/angular/controllers/*.js', ['librerias', 'libreriasPublic'])
  gulp.watch('./assets/angular/*.js', ['localAngular'])
  gulp.watch('./views/**', ['html'])
  gulp.watch('./assets/angular/modules/**/**.html', ['templates'])
  gulp.watch('./assets/angular/modules/**/**.js', ['angularModules'])
  gulp.watch('./assets/angular/factories/**/**.js', ['angularFactories'])
  gulp.watch(['./assets/css/**/*.css', './assets/css/**/*.scss'], ['css'])
})