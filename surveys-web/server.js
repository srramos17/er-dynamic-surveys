const express = require('express')
const config = require('./config')
const bodyparser = require('body-parser')
const expressSession = require('express-session')
const passport = require('passport')
const helmet = require('helmet');
const cuestionarios = require('surveys-client')
const auth = require('./auth')
const app = express()
const port = process.env.PORT || 3000

const client = cuestionarios.CreateClient(config.client)

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html')
app.set('views', `${__dirname}/public/views`);

app.use(bodyparser.json())
app.use(express.static(`${__dirname}/public`))
app.use(helmet());
app.use(bodyparser.urlencoded({extended: false}))
app.use(expressSession({
  secret: config.secret,
  resave: false,
  saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())

passport.use(auth.localStrategy)
passport.deserializeUser(auth.deserializeUser)
passport.serializeUser(auth.serializeUser);

app.get('/', (req, res) => {
  res.render('index');
})

app.get('/login', (req, res) => {
  res.render('index');
})

app.get('/logout', (req, res) => {
  try {
    req.logout()
    res.status(201).send({message: 'User is logout'})
  }
  catch (e) {
    res.status(500).send({error: `an error has ocurred: ${e.message}`})
  }
})

app.get('/cd(/)?*', ensureAuth, (req, res) => {
  res.render('home');
})

app.post('/auth', passport.authenticate('local', {
  successRedirect: '/cd',
  failureRedirect: '/login'
}))

app.get('/getDocuments', ensureAuth, (req, res) => {
  const token = req.user.token
  let user ={
    userId: req.user.userId
  }

  client.getDocuments(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.get('/getCompanies', ensureAuth, (req, res) => {
  const token = req.user.token
  const user ={
    userId: req.user.userId,
    company: req.user.company
  }

  client.getCompanies(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.get('/getBranchesByCompany/:idCompany', ensureAuth, (req, res) => {
  const idCompany = req.params.idCompany
  const token = req.user.token
  const user ={
    userId: req.user.userId,
    companyId: idCompany,
  }

  client.getBranchesByCompany(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/user', ensureAuth, (req, res) => {
  let user = req.body
  const token = req.user.token
  user.userId = req.user.userId
  user.verified = true

  client.createUser(token, user, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/updateUser', ensureAuth, (req, res) => {
  let user = req.body
  const token = req.user.token
  user.userId = req.user.userId

  client.updateUser(token, user, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.get('/getUsers', ensureAuth, (req, res) => {
  const token = req.user.token
  let user ={
    userId: req.user.userId,
    company: req.user.company,
    branch: req.user.branch,
    rol: req.user.rol
  }

  client.getUsers(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.get('/user/:id', ensureAuth, (req, res) => {
  const id = req.params.id
  const userId = req.user.userId
  const token = req.user.token
  const user ={
    userId: userId
  }

  client.getUserById(user, token, id, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/dropUser', ensureAuth, (req, res) => {
  const users = req.body
  const userId = req.user.userId
  const token = req.user.token
  const user ={
    userId: userId,
    users: users
  }

  client.deleteUser(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

//QUITAR
app.get('/whoami', function (req, res) {
  if (req.isAuthenticated()) {
    return res.json((req.user))
  }

  return res.json({ auth: false})
})

app.get('/getPolles', ensureAuth, (req, res) => {
  const token = req.user.token
  let user ={
    userId: req.user.userId,
    company: req.user.company,
    branch: req.user.branch
  }

  client.listSurveys(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.get('/poll/:id', ensureAuth, (req, res) => {
  const id = req.params.id
  const userId = req.user.userId
  const token = req.user.token
  const user ={
    userId: userId
  }

  client.getSurveyById(user, token, id, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/poll', ensureAuth, (req, res) => {
  const poll = req.body
  const token = req.user.token

  poll.userId = req.user.userId
  poll.company = req.user.company
  poll.branch = req.user.branch

  client.createSurvey(poll, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/updatePoll', ensureAuth, (req, res) => {
  const poll = req.body
  const userId = req.user.userId
  const token = req.user.token
  poll.userId = userId

  client.updateSurvey(poll, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/dropPoll', ensureAuth, (req, res) => {
  const polles = req.body
  const userId = req.user.userId
  const token = req.user.token
  const user ={
    userId: userId,
    polles: polles
  }

  client.deleteSurvey(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.get('/getRoles', ensureAuth, (req, res) => {
  const token = req.user.token
  const user ={
    userId: req.user.userId,
    company: req.user.company,
    branch: req.user.branch
  }

  client.listRoles(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.get('/role/:id', ensureAuth, (req, res) => {
  const id = req.params.id
  const userId = req.user.userId
  const token = req.user.token
  const user ={
    userId: userId
  }

  client.getRoleById(user, token, id, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/role', ensureAuth, (req, res) => {
  const role = req.body
  const token = req.user.token

  role.userId = req.user.userId
  role.company = req.user.company

  client.createRole(role, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/updateRole', ensureAuth, (req, res) => {
  const role = req.body
  const token = req.user.token
  role.userId = req.user.userId

  client.updateRole(role, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/dropRole', ensureAuth, (req, res) => {
  const roles = req.body
  const userId = req.user.userId
  const token = req.user.token
  const user ={
    userId: userId,
    roles: roles
  }

  client.deleteRole(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.get('/getGroups', ensureAuth, (req, res) => {
  const token = req.user.token
  let user ={
    userId: req.user.userId,
    company: req.user.company,
    branch: req.user.branch
  }

  client.listGroups(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.get('/group/:id', ensureAuth, (req, res) => {
  const id = req.params.id
  const userId = req.user.userId
  const token = req.user.token
  const user ={
    userId: userId
  }

  client.getGroupById(user, token, id, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/group', ensureAuth, (req, res) => {
  const group = req.body
  const token = req.user.token

  group.userId = req.user.userId
  group.company = req.user.company
  group.branch = req.user.branch

  client.createGroup(group, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/updateGroup', ensureAuth, (req, res) => {
  const group = req.body
  const userId = req.user.userId
  const token = req.user.token
  group.userId = userId

  client.updateGroup(group, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/dropGroup', ensureAuth, (req, res) => {
  const groups = req.body
  const userId = req.user.userId
  const token = req.user.token
  const user ={
    userId: userId,
    groups: groups
  }

  client.deleteGroup(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.get('/getQuestions', ensureAuth, (req, res) => {
  const token = req.user.token
  const user ={
    userId: req.user.userId,
    company: req.user.company,
    branch: req.user.branch
  }

  client.listQuestions(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.get('/question/:id', ensureAuth, (req, res) => {
  const id = req.params.id
  const userId = req.user.userId
  const token = req.user.token
  const user ={
    userId: userId
  }

  client.getQuestionById(user, token, id, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/question', ensureAuth, (req, res) => {
  const question = req.body
  const token = req.user.token

  question.userId = req.user.userId
  question.company = req.user.company
  question.branch = req.user.branch

  client.createQuestion(question, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/updateQuestion', ensureAuth, (req, res) => {
  const question = req.body
  const token = req.user.token
  question.userId = req.user.userId

  client.updateQuestion(question, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

app.post('/dropQuestion', ensureAuth, (req, res) => {
  const questions = req.body
  const userId = req.user.userId
  const token = req.user.token
  const user ={
    userId: userId,
    questions: questions
  }

  client.deleteQuestion(user, token, (err, dt) => {
    if(err){
      res.status(400).send(err)
    }

    res.send(dt)
  })
})

function ensureAuth (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  res.redirect('/')
}

app.listen(port, () => {
  console.log(`Servidor escuchando en el puerto ${port}`)
})