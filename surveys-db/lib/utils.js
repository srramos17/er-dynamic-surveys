'use strict'

const crypto = require('crypto')

const utils = {
  encrypt
}

function encrypt (valor) {
  let shasum = crypto.createHash('sha256')
  shasum.update(valor)
  return shasum.digest('hex')
}

module.exports = utils
