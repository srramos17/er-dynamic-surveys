'use strict'

const co = require('co')
const r = require('rethinkdb')
const Promise = require('bluebird')
const utils = require('./utils')
const uuid = require('uuid-base62')

const defaultsdb = {
  host: '127.0.0.1',
  port: 28015,
  db: 'dbCuestionarios'
}

class Db {
  constructor (options) {
    this.host = options.host || defaultsdb.host
    this.port = options.port || defaultsdb.port
    this.db = options.bd || defaultsdb.db
    this.setup = options.setup || false
  }

  connect (callback) {
    this.connection = r.connect({
      host: this.host,
      port: this.port
    })

    this.connected = true
    let db = this.db
    let connection = this.connection

    if (!this.setup) {
      return Promise.resolve(connection).asCallback(callback)
    }

    let setup = co.wrap(function * () {
      let conn = yield connection

      let dbList = yield r.dbList().run(conn)

      if (dbList.indexOf(db) === -1) {
        yield r.dbCreate(db).run(conn)
      }

      let dbTables = yield r.db(db).tableList().run(conn)

      if (dbTables.indexOf('surveys') === -1) {
        yield r.db(db).tableCreate('surveys').run(conn)
        yield r.db(db).table('surveys').indexCreate('name', {multi: true}).run(conn)
        yield r.db(db).table('surveys').indexCreate('publicId').run(conn)
        yield r.db(db).table('surveys').indexCreate('company', {multi: true}).run(conn)
        yield r.db(db).table('surveys').indexCreate('branch', {multi: true}).run(conn)
      }

      if (dbTables.indexOf('groups') === -1) {
        yield r.db(db).tableCreate('groups').run(conn)
        yield r.db(db).table('groups').indexCreate('publicId').run(conn)
      }

      if (dbTables.indexOf('questions') === -1) {
        yield r.db(db).tableCreate('questions').run(conn)
        yield r.db(db).table('questions').indexCreate('description', {multi: true}).run(conn)
        yield r.db(db).table('questions').indexCreate('publicId').run(conn)
      }

      if (dbTables.indexOf('answersType') === -1) {
        yield r.db(db).tableCreate('answersType').run(conn)
        yield r.db(db).table('answersType').indexCreate('publicId').run(conn)
      }

      if (dbTables.indexOf('users') === -1) {
        yield r.db(db).tableCreate('users').run(conn)
        yield r.db(db).table('users').indexCreate('username', {multi: true}).run(conn)
        yield r.db(db).table('users').indexCreate('company', {multi: true}).run(conn)
        yield r.db(db).table('users').indexCreate('branch', {multi: true}).run(conn)
      }

      if (dbTables.indexOf('roles') === -1) {
        yield r.db(db).tableCreate('roles').run(conn)
        yield r.db(db).table('roles').indexCreate('publicId').run(conn)
      }

      if (dbTables.indexOf('patients') === -1) {
        yield r.db(db).tableCreate('patients').run(conn)
        yield r.db(db).table('patients').indexCreate('publicId').run(conn)
      }

      if (dbTables.indexOf('companies') === -1) {
        yield r.db(db).tableCreate('companies').run(conn)
        yield r.db(db).table('companies').indexCreate('publicId').run(conn)
      }

      if (dbTables.indexOf('branches') === -1) {
        yield r.db(db).tableCreate('branches').run(conn)
        yield r.db(db).table('branches').indexCreate('publicId').run(conn)
      }

      if (dbTables.indexOf('countries') === -1) {
        yield r.db(db).tableCreate('countries').run(conn)
        yield r.db(db).table('countries').indexCreate('publicId').run(conn)
      }

      if (dbTables.indexOf('cities') === -1) {
        yield r.db(db).tableCreate('cities').run(conn)
        yield r.db(db).table('cities').indexCreate('publicId').run(conn)
      }

      if (dbTables.indexOf('documentTypes') === -1) {
        yield r.db(db).tableCreate('documentTypes').run(conn)
      }

      // if (dbTables.indexOf('answersValues') === -1) {
      //   yield r.db(db).tableCreate('answersValues').run(conn)
      //   yield r.db(db).table('answersValues').indexCreate('answerValueId').run(conn)
      // }

      // if (dbTables.indexOf('answers') === -1) {
      //   yield r.db(db).tableCreate('answers').run(conn)
      //   yield r.db(db).table('answers').indexCreate('surveyId', {multi: true}).run(conn)
      //   yield r.db(db).table('answers').indexCreate('groupId', {multi: true}).run(conn)
      //   yield r.db(db).table('answers').indexCreate('answersId').run(conn)
      //   yield r.db(db).table('answers').indexCreate('answerDate', {multi: true}).run(conn)
      //   yield r.db(db).table('answers').indexCreate('userId', {multi: true}).run(conn)
      //   yield r.db(db).table('answers').indexCreate('patientId', {multi: true}).run(conn)
      // }

      return conn
    })

    return Promise.resolve(setup()).asCallback(callback)
  }

  disconnect (callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    this.connected = false
    return Promise.resolve(this.connection)
    .then((conn) => conn.close())
  }

  saveSurvey (survey, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null
      let saved = null

      try {
        result = yield r.db(db).table('surveys').insert(survey).run(conn)
      } catch (e) {
        if (result.errors > 0) {
          return Promise.reject(new Error(result.first_error))
        }
      }

      survey.id = result.generated_keys[0]
      try {
        result = yield r.db(db).table('surveys').get(survey.id).update({
          publicId: uuid.encode(survey.id),
          deleted: false
        }).run(conn)

        saved = yield r.db(db).table('surveys').get(survey.id).run(conn)

      } catch (e) {
        if (result.errors > 0) {
          return Promise.reject(new Error(result.first_error))
        }
      }

      return Promise.resolve(saved)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  updateSurvey (survey, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    if (!survey) {
      return Promise.reject(new Error('not survey to update')).asCallback(callback)
    }

    const connection = this.connection
    const db = this.db

    let task = co.wrap(function * () {
      const conn = yield connection
      let result = null
      const surveyId = uuid.decode(survey.publicId)

      try {
        yield r.db(db).table('surveys').get(surveyId).update(survey).run(conn)
        result = yield r.db(db).table('surveys').get(surveyId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getSurveys (user, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection

      let result = yield r.db(db).table('surveys').filter({deleted: false, company: user.company, branch: user.branch}).run(conn)
      let surveys = null

      try {
        surveys = yield result.toArray()
      } catch (e) {
        return Promise.reject(new Error('Cannot request surveys'))
      }

      return Promise.resolve(surveys)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getSurveyById (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let surveyId = uuid.decode(id)
      let result = yield r.db(db).table('surveys').get(surveyId).run(conn)

      if (result.errors > 0) {
        return Promise.reject(new Error(result.first_error))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  deleteSurvey (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null
      yield r.db(db).table('surveys').indexWait().run(conn)
      let surveyId = uuid.decode(id)

      try {
        yield r.db(db).table('surveys').get(surveyId).update({
          deleted: true,
          active: false
        }).run(conn)

        result = yield r.db(db).table('surveys').get(surveyId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  saveGroup (group, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null
      let saved = null

      try {
        result = yield r.db(db).table('groups').insert(group).run(conn)
      } catch (e) {
        if (result.errors > 0) {
          return Promise.reject(new Error(result.first_error))
        }
      }

      group.id = result.generated_keys[0]
      try {
        result = yield r.db(db).table('groups').get(group.id).update({
          publicId: uuid.encode(group.id),
          deleted: false
        }).run(conn)

        saved = yield r.db(db).table('groups').get(group.id).run(conn)

      } catch (e) {
        if (result.errors > 0) {
          return Promise.reject(new Error(result.first_error))
        }
      }

      return Promise.resolve(saved)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getGroups (user, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected'))
    }

    let connection = this.connection
    let db = this.db
    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null

      try {
        result = yield r.db(db).table('groups').filter({deleted: false, company: user.company, branch: user.branch}).run(conn)
        result = yield result.toArray()
      } catch (e) {
        return Promise.reject(new Error(e.message))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getGroupById (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let decodeId = uuid.decode(id)
      let result = yield r.db(db).table('groups').get(decodeId).run(conn)

      if (result.errors > 0) {
        return Promise.reject(new Error(result.first_error))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }


  updateGroup (group, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    if (!group) {
      return Promise.reject(new Error('not group to update')).asCallback(callback)
    }

    const connection = this.connection
    const db = this.db

    let task = co.wrap(function * () {
      const conn = yield connection
      let result = null
      const decodeId = uuid.decode(group.publicId)

      try {
        yield r.db(db).table('groups').get(decodeId).update(group).run(conn)
        result = yield r.db(db).table('groups').get(decodeId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  deleteGroup (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let decodeId = uuid.decode(id)
      let result = null

      try {
        yield r.db(db).table('groups').get(decodeId).update({
          deleted: true,
          active: false
        }).run(conn)

        result = yield r.db(db).table('groups').get(decodeId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  saveUser (user, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let pass = user.password
      user.password = utils.encrypt(pass)
      user.company = uuid.decode(user.companyId)
      user.branch = uuid.decode(user.branchId)
      user.rol = uuid.decode(user.roleId)
      delete user.companyId
      delete user.branchId
      delete user.roleId

      let result = yield r.db(db).table('users').insert(user).run(conn)

      if (result.errors > 0) {
        return Promise.reject(new Error(result.first_error))
      }

      user.id = result.generated_keys[0]

      result = yield r.db(db).table('users').get(user.id).update({
        publicId: uuid.encode(user.id),
        deleted: false
      }).run(conn)

      if (result.errors > 0) {
        return Promise.reject(new Error(result.first_error))
      }

      let saved = yield r.db(db).table('users').get(user.id).run(conn)

      return Promise.resolve(saved)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  authUser (username, password, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db
    let task = co.wrap(function * () {
      let conn = yield connection
      let encryptPassword = utils.encrypt(password)

      yield r.db(db).table('users').indexWait().run(conn)

      let users = yield r.db(db).table('users').getAll(username, {
        index: 'username'
      }).run(conn)
      let result = null

      try {
        result = yield users.next()
      } catch (e) {
        return Promise.resolve(false)
      }

      if (result.password === encryptPassword) {
        return Promise.resolve(true)
      }

      return Promise.resolve(false)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getUsers (user, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database'))
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null
      let rol = null
      let users = null

      rol = yield r.db(db).table('roles').get(user.rol).getField('role').run(conn)

      if (rol === 'Administrador') {
        result = yield r.db(db).table('users')
          .eqJoin('company', r.db(db).table('companies'))
          .map(function (val){
            return val('left').merge({
              companyName: val('right')('name')
            })
          })
          .eqJoin('branch', r.db(db).table('branches'))
          .map(function (res) {
            return res('left').merge({
              branchName: res('right')('name')
            })
          })
          .eqJoin('rol', r.db(db).table('roles'))
          .map(function (res) {
            return res('left').merge({
              roleName: res('right')('role')
            })
          })
          .filter({deleted:false, company: user.company}).run(conn)
      } else {
        result = result = yield r.db(db).table('users')
        .eqJoin('company', r.db(db).table('companies'))
        .map(function (val){
          return val('left').merge({
            companyName: val('right')('name')
          })
        })
        .eqJoin('branch', r.db(db).table('branches'))
        .map(function (res) {
          return res('left').merge({
            branchName: res('right')('name')
          })
        })
        .eqJoin('rol', r.db(db).table('roles'))
        .map(function (res) {
          return res('left').merge({
            roleName: res('right')('role')
          })
        }).filter({deleted:false, company: user.company, branch: user.branch}).run(conn)
      }

      try {
        users = yield result.toArray()
      } catch (e) {
        return Promise.reject(new Error('cannot get users'))
      }

      return Promise.resolve(users)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getUserById (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let userId = uuid.decode(id)
      let user = null
      yield r.db(db).table('users').indexWait().run(conn)

      let result = yield r.db(db).table('users')
      .eqJoin('company', r.db(db).table('companies'))
      .map(function (val){
        return val('left').merge({
          companyName: val('right')('name'),
          companyId: val('right')('publicId')
        })
      })
      .eqJoin('branch', r.db(db).table('branches'))
      .map(function (res) {
        return res('left').merge({
          branchName: res('right')('name'),
          branchId: res('right')('publicId')
        })
      })
      .eqJoin('rol', r.db(db).table('roles'))
      .map(function (res) {
        return res('left').merge({
          roleName: res('right')('role'),
          roleId: res('right')('publicId')
        })
      }).filter({deleted:false, id: userId}).run(conn)

      if (result.errors > 0) {
        return Promise.reject(new Error(result.first_error))
      }

      try {
        user = yield result.toArray()
      } catch (e) {
        return Promise.reject(new Error('cannot get user data'))
      }

      return Promise.resolve(user)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getUser (username, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db
    let task = co.wrap(function * () {
      let conn = yield connection

      yield r.db(db).table('users').indexWait().run(conn)

      let users = yield r.db(db).table('users').getAll(username, {
        index: 'username'
      }).run(conn)
      let result = null

      try {
        result = yield users.next()
      } catch (e) {
        return Promise.reject(new Error(`user ${username} not found`))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  updateUser (user, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    if (!user) {
      return Promise.reject(new Error('not user to update')).asCallback(callback)
    }

    const connection = this.connection
    const db = this.db

    let task = co.wrap(function * () {
      const conn = yield connection
      let result = null
      const userId = uuid.decode(user.publicId)

      try {
        yield r.db(db).table('users').get(userId).update(user).run(conn)
        result = yield r.db(db).table('users').get(userId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  deleteUser (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let decodeId = uuid.decode(id)
      let result = null

      try {
        yield r.db(db).table('users').get(decodeId).update({
          deleted: true,
          active: false
        }).run(conn)

        result = yield r.db(db).table('users').get(decodeId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  saveQuestion (question, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null
      let saved = null

      try {
        result = yield r.db(db).table('questions').insert(question).run(conn)
      } catch (e) {
        if (result.errors > 0) {
          return Promise.reject(new Error(result.first_error))
        }
      }

      question.id = result.generated_keys[0]
      try {
        result = yield r.db(db).table('questions').get(question.id).update({
          publicId: uuid.encode(question.id),
          deleted: false
        }).run(conn)

        saved = yield r.db(db).table('questions').get(role.id).run(conn)

      } catch (e) {
        if (result.errors > 0) {
          return Promise.reject(new Error(result.first_error))
        }
      }

      return Promise.resolve(saved)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getQuestions (user, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database'))
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null
      let rol = null
      let questions = null

      yield r.db(db).table('questions').indexWait().run(conn)

      result = result = yield r.db(db).table('questions')
       .eqJoin('company', r.db(db).table('companies'))
       .map(function (val){
         return val('left').merge({
           companyName: val('right')('name')
         })
       })
      .eqJoin('branch', r.db(db).table('branches'))
      .map(function (res) {
        return res('left').merge({
          branchName: res('right')('name')
        })
      })
      .filter({deleted:false, company: user.company, branch: user.branch}).run(conn)

      try {
        questions = yield result.toArray()
      } catch (e) {
        return Promise.reject(new Error('cannot get questions'))
      }

      return Promise.resolve(questions)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getQuestionById (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let questionId = uuid.decode(id)
      let question = null
      yield r.db(db).table('questions').indexWait().run(conn)

      let result = yield r.db(db).table('questions')
      .eqJoin('company', r.db(db).table('companies'))
      .map(function (val){
        return val('left').merge({
          companyName: val('right')('name'),
          companyId: val('right')('publicId')
        })
      })
      .eqJoin('branch', r.db(db).table('branches'))
      .map(function (res) {
        return res('left').merge({
          branchName: res('right')('name'),
          branchId: res('right')('publicId')
        })
      })
      .filter({deleted:false, id: questionId}).run(conn)

      if (result.errors > 0) {
        return Promise.reject(new Error(result.first_error))
      }

      try {
        question = yield result.toArray()
      } catch (e) {
        return Promise.reject(new Error('cannot get question data'))
      }

      return Promise.resolve(question)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  updateQuestion (question, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    if (!question) {
      return Promise.reject(new Error('not question to update')).asCallback(callback)
    }

    const connection = this.connection
    const db = this.db

    let task = co.wrap(function * () {
      const conn = yield connection
      let result = null
      const decodeId = uuid.decode(question.publicId)

      try {
        yield r.db(db).table('questions').get(decodeId).update(question).run(conn)
        result = yield r.db(db).table('questions').get(decodeId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  deleteQuestion (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let decodeId = uuid.decode(id)
      let result = null

      try {
        yield r.db(db).table('questions').get(decodeId).update({
          deleted: true,
          active: false
        }).run(conn)

        result = yield r.db(db).table('questions').get(decodeId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  saveRole (role, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null
      let saved = null

      try {
        result = yield r.db(db).table('roles').insert(role).run(conn)
      } catch (e) {
        if (result.errors > 0) {
          return Promise.reject(new Error(result.first_error))
        }
      }

      role.id = result.generated_keys[0]
      try {
        result = yield r.db(db).table('roles').get(role.id).update({
          publicId: uuid.encode(role.id),
          deleted: false
        }).run(conn)

        saved = yield r.db(db).table('roles').get(role.id).run(conn)

      } catch (e) {
        if (result.errors > 0) {
          return Promise.reject(new Error(result.first_error))
        }
      }

      return Promise.resolve(saved)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getRoles (user, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected'))
    }

    let connection = this.connection
    let db = this.db
    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null

      try {
        result = yield r.db(db).table('roles').filter({deleted: false, company: user.company}).run(conn)
        result = yield result.toArray()
      } catch (e) {
        return Promise.reject(new Error(e.message))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getRoleById (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let decodeId = uuid.decode(id)
      let result = yield r.db(db).table('roles').get(decodeId).run(conn)

      if (result.errors > 0) {
        return Promise.reject(new Error(result.first_error))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  updateRole (role, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    if (!role) {
      return Promise.reject(new Error('not role to update')).asCallback(callback)
    }

    const connection = this.connection
    const db = this.db

    let task = co.wrap(function * () {
      const conn = yield connection
      let result = null
      const decodeId = uuid.decode(role.publicId)

      try {
        yield r.db(db).table('roles').get(decodeId).update(role).run(conn)
        result = yield r.db(db).table('roles').get(decodeId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  deleteRole (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let decodeId = uuid.decode(id)
      let result = null

      try {
        yield r.db(db).table('roles').get(decodeId).update({
          deleted: true,
          active: false
        }).run(conn)

        result = yield r.db(db).table('roles').get(decodeId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  saveAnswerType (type, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    if (!type) {
      return Promise.reject(new Error('not type found to save'))
    }
    let connection = this.connection
    let db = this.db
    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null

      try {
        result = yield r.db(db).table('answersType').insert(type).run(conn)
        type.id = result.generated_keys[0]

        yield r.db(db).table('answersType').get(type.id).update({
          publicId: uuid.encode(type.id)
        }).run(conn)

        result = yield r.db(db).table('answersType').get(type.id).run(conn)
      } catch (e) {
        return Promise.reject(new Error('cannot save answerType'))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getAnswersTypes (callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db
    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null

      try {
        result = yield r.db(db).table('answersType').run(conn)
        result = yield result.toArray()
      } catch (e) {
        return Promise.reject(new Error(`cannot get answersTypes.`))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getAnswerTypeById (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected'))
    }

    if (!id) {
      return Promise.reject(new Error('answerType id not found'))
    }
    let connection = this.connection
    let db = this.db
    let answerId = uuid.decode(id)
    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null

      try {
        result = yield r.db(db).table('answersType').get(answerId).run(conn)
      } catch (e) {
        return Promise.reject(new Error('cannot get answer Type'))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  updateAnswerType (answer, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    if (!answer) {
      return Promise.reject(new Error('not answerType to update')).asCallback(callback)
    }

    const connection = this.connection
    const db = this.db

    let task = co.wrap(function * () {
      const conn = yield connection
      let result = null
      const answerId = uuid.decode(answer.publicId)

      try {
        yield r.db(db).table('answersType').get(answerId).update(answer).run(conn)
        result = yield r.db(db).table('answersType').get(answerId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  deleteAnswerType (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database')).asCallback(callback)
    }

    let connection = this.connection
    let db = this.db

    let task = co.wrap(function * () {
      let conn = yield connection
      let answerId = uuid.decode(id)
      let result = yield r.db(db).table('answersType').get(answerId).run(conn)

      if (result.errors > 0) {
        return Promise.reject(new Error(result.first_error))
      }

      try {
        yield r.db(db).table('answersType').get(answerId).update({
          deleted: true
        }).run(conn)
        result = yield r.db(db).table('answersType').get(answerId).run(conn)
      } catch (e) {
        return Promise.reject(new Error(e.message)).asCallback(callback)
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getTypeDocuments (callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database'))
    }

    let connection = this.connection
    let db = this.db
    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null

      try {
        result = yield r.db(db).table('documentTypes').run(conn)
        result = yield result.toArray()
      } catch (e) {
        return Promise.reject(new Error(e.message))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getCompanies (user, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected to database'))
    }

    let connection = this.connection
    let db = this.db
    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null

      try {
        result = yield r.db(db).table('companies')
        .filter({id: user.company}).run(conn)

        result = yield result.toArray()
      } catch (e) {
        return Promise.reject(new Error(e.message))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }

  getBranchesByCompany (user, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected'))
    }

    let connection = this.connection
    let db = this.db
    let task = co.wrap(function * () {
      let conn = yield connection
      let result = null
      let idCompany = uuid.decode(user.companyId)

      try {
        result = yield r.db(db).table('branches')
        .filter({deleted:false, company: idCompany}).run(conn)
        result = yield result.toArray()
      } catch (e) {
        return Promise.reject(new Error(e.message))
      }

      return Promise.resolve(result)
    })

    return Promise.resolve(task()).asCallback(callback)
  }
}

module.exports = Db
