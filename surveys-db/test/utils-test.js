'use stric'

const utils = require('../lib/utils')
const test = require('ava')

test('encrypt password', async t => {
  t.is(typeof utils.encrypt, 'function', 'encrypt should be a function')
  let password = 'foo123'
  let encrypted = '02b353bf5358995bc7d193ed1ce9c2eaec2b694b21d2f96232c9d6a0832121d1'

  let result = utils.encrypt(password)
  t.is(result, encrypted)
})
