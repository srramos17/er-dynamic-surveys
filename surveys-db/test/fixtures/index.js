'use strict'

const fixtures = {
  getSurvey () {
    return {
      name: 'Survey name',
      active: true,
      deleted: false
    }
  },
  getSurveys (n) {
    let surveys = []

    for (let i = 0; i < n; i++) {
      surveys.push(this.getSurvey())
    }

    return surveys
  },
  getGroup () {
    return {
      name: 'Group name',
      active: true,
      deleted: false
    }
  },
  getGroups (n) {
    let groups = []

    for (let i = 0; i < n; i++) {
      groups.push(this.getGroup())
    }

    return groups
  },
  getUser () {
    return {
      name: 'User firstname',
      lastname: 'user lastname',
      username: 'username',
      email: 'email@test.co',
      password: 'foo123',
      rol: '1',
      companyId: 1,
      branchId: 1,
      active: true,
      deleted: false
    }
  },
  getUsers (n) {
    let users = []

    for (let i = 0; i < n; i++) {
      users.push(this.getUser())
    }

    return users
  },
  getQuestion () {
    return {
      description: 'how much God loved world',
      answerTypeId: '1',
      maxLength: null,
      dateFormat: null,
      isNumeric: false,
      decimalPrecision: 0,
      decimalScale: 0,
      minRange: 0,
      required: true,
      applyNote: false,
      active: true,
      deleted: false
    }
  },
  getQuestions (n) {
    let questions = []
    for (let i = 0; i < 0; i++) {
      questions.push(this.getQuestion())
    }

    return questions
  },
  getRole () {
    return {
      role: 'Admin',
      active: true,
      deleted: false
    }
  },
  getRoles (n) {
    let roles = []
    for (let i = 0; i < 0; i++) {
      roles.push(this.getRole())
    }

    return roles
  },
  getAnswerType () {
    return {
      type: 'numerico',
      active: true,
      deleted: false
    }
  },
  getAnwersTypes () {
    return [
      this.getAnswerType()
    ]
  }
}

module.exports = fixtures
