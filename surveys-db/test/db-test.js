'use strict'

const test = require('ava')
const uuid = require('uuid-base62')
const r = require('rethinkdb')
const Db = require('../')
const fixtures = require('./fixtures')

test.beforeEach('setup database', async t => {
  const dbName = `dbCuestionarios_${uuid.v4()}`
  const db = new Db({ bd: dbName, setup: true })
  await db.connect()

  t.context.db = db
  t.context.dbName = dbName

  t.true(db.connected, 'should be connected')
})

test.afterEach.always('cleanup database', async t => {
  let db = t.context.db
  let dbName = t.context.dbName

  await db.disconnect()
  t.false(db.connected, 'should be disconnected')

  let conn = await db.connect()
  await r.dbDrop(dbName).run(conn)
})

test('save survey', async t => {
  let db = t.context.db

  t.is(typeof db.saveSurvey, 'function', 'saveSurvey should be a function')

  let survey = fixtures.getSurvey()
  let result = await db.saveSurvey(survey)

  t.is(survey.name, result.name)
  t.is(survey.active, result.active)
  t.is(survey.deleted, result.deleted)
})

test('get surveys', async t => {
  let db = t.context.db
  t.is(typeof db.getSurveys, 'function', 'getSurveys should be a function')

  let surveys = fixtures.getSurveys(1)
  let saveSurveys = surveys.map(l => db.saveSurvey(l))
  let saved = await Promise.all(saveSurveys)
  let result = await db.getSurveys()

  t.is(saved.length, result.length)
})

test('get survey by id', async t => {
  let db = t.context.db
  t.is(typeof db.getSurveyById, 'function', 'getSurveyById should be a function')

  let survey = fixtures.getSurvey()
  let saved = await db.saveSurvey(survey)
  let result = await db.getSurveyById(saved.publicId)

  t.deepEqual(saved, result)
})

test('update survey', async t => {
  const db = t.context.db
  t.is(typeof db.updateSurvey, 'function', 'updateSurvey should be a function')

  const fixture = fixtures.getSurvey()
  const saved = await db.saveSurvey(fixture)
  const survey = await db.getSurveyById(saved.publicId)
  const result = await db.updateSurvey(survey)

  t.is(survey.name, result.name)
  t.is(survey.active, result.active)
  t.is(survey.deleted, result.deleted)
})

test('delete survey', async t => {
  let db = t.context.db
  t.is(typeof db.deleteSurvey, 'function', 'deleteSurvey should be a function')

  const fixture = fixtures.getSurvey()
  const saved = await db.saveSurvey(fixture)
  const select = await db.getSurveyById(saved.publicId)
  const deleted = await db.deleteSurvey(select.publicId)
  const result = await db.getSurveyById(saved.publicId)

  t.true(deleted.deleted)
  t.is(deleted.deleted, result.deleted)
})

test('get survey by name', async t => {
  let db = t.context.db
  t.is(typeof db.getSurveyByName, 'function', 'getSurveyByName should be a function')

  let survey = fixtures.getSurvey()
  let saved = await db.saveSurvey(survey)
  let result = await db.getSurveyByName(saved.name)

  t.deepEqual(saved, result)
})

test('save group', async t => {
  let db = t.context.db
  t.is(typeof db.saveGroup, 'function', 'saveGroup should be a function')

  let group = fixtures.getGroup()
  let result = await db.saveGroup(group)

  t.is(group.name, result.name)
})

test('update group', async t => {
  let db = t.context.db
  t.is(typeof db.updateGroup, 'function', 'updateGroup should be a function')

  let group = fixtures.getGroup()
  let saved = await db.saveGroup(group)
  let get = await db.getGroupById(saved.publicId)
  get.active = false

  let updated = await db.updateGroup(get)

  t.is(get.active, updated.active)
  t.false(updated.active)
})

test('delete group', async t => {
  let db = t.context.db
  t.is(typeof db.deleteGroup, 'function', 'deleteGroup should be a function')

  let group = fixtures.getGroup()
  let saved = await db.saveGroup(group)
  let get = await db.getGroupById(saved.publicId)
  let deleted = await db.deleteGroup(get.publicId)

  t.true(deleted.deleted)
})

test('get groups', async t => {
  let db = t.context.db
  t.is(typeof db.saveGroup, 'function', 'saveGroup should be a function')

  let groups = fixtures.getGroups(1)
  let savedGroups = groups.map(l => db.saveGroup(l))
  let saved = await Promise.all(savedGroups)
  let result = await db.getGroups()

  t.is(saved.length, result.length)
})

test('get group by id', async t => {
  let db = t.context.db
  t.is(typeof db.saveGroup, 'function', 'saveGroup should be a function')

  let group = fixtures.getGroup()
  let saved = await db.saveGroup(group)
  let result = await db.getGroupById(saved.publicId)

  t.deepEqual(saved, result)
})

test('save user', async t => {
  let db = t.context.db
  t.is(typeof db.saveUser, 'function', 'saveUser should be a function')

  let user = fixtures.getUser()
  let result = await db.saveUser(user)

  t.is(user.name, result.name)
  t.is(user.lastname, result.lastname)
  t.is(user.username, result.username)
  t.is(user.email, result.email)
})

test('update user', async t => {
  let db = t.context.db
  t.is(typeof db.updateUser, 'function', 'updateUser should be a function')

  let user = fixtures.getUser()
  let saved = await db.saveUser(user)
  let get = await db.getUserById(saved.publicId)
  get.active = false

  let updated = await db.updateUser(get)

  t.is(get.active, updated.active)
  t.false(updated.active)
})

test('delete user', async t => {
  let db = t.context.db
  t.is(typeof db.deleteUser, 'function', 'deleteUser should be a function')

  let user = fixtures.getUser()
  let saved = await db.saveUser(user)
  let get = await db.getUserById(saved.publicId)
  let deleted = await db.deleteUser(get.publicId)

  t.true(deleted.deleted)
})

test('get users', async t => {
  let db = t.context.db
  t.is(typeof db.getUsers, 'function', 'should be a function')

  let users = fixtures.getUsers(1)
  let savedUsers = users.map(l => db.saveUser(l))
  let saved = await Promise.all(savedUsers)
  let result = await db.getUsers()

  t.is(saved.length, result.length)
})

test('get user by id', async t => {
  let db = t.context.db
  t.is(typeof db.getUserById, 'function', ' should be a function')

  let user = fixtures.getUser()
  let saved = await db.saveUser(user)
  let result = await db.getUserById(saved.publicId)

  t.deepEqual(saved, result)
})

test('save question', async t => {
  let db = t.context.db
  t.is(typeof db.saveQuestion, 'function', 'saveQuestion should be a function')

  let q = fixtures.getQuestion()
  let saveQ = await db.saveQuestion(q)

  t.is(q.description, saveQ.description)
  t.is(q.answerTypeId, saveQ.answerTypeId)
  t.is(q.isNumeric, saveQ.isNumeric)
  t.is(q.required, saveQ.required)
  t.is(q.minRange, saveQ.minRange)
})

test('update question', async t => {
  let db = t.context.db
  t.is(typeof db.updateQuestion, 'function', 'updateQuestion should be a function')

  let q = fixtures.getQuestion()
  let saved = await db.saveQuestion(q)
  let get = await db.getQuestionById(saved.publicId)
  get.active = false

  let updated = await db.updateQuestion(get)

  t.is(get.active, updated.active)
  t.false(updated.active)
})

test('delete question', async t => {
  let db = t.context.db
  t.is(typeof db.deleteQuestion, 'function', 'deleteQuestion should be a function')

  let q = fixtures.getQuestion()
  let saved = await db.saveQuestion(q)
  let get = await db.getQuestionById(saved.publicId)
  let deleted = await db.deleteQuestion(get.publicId)

  t.true(deleted.deleted)
})

test('get questions', async t => {
  let db = t.context.db
  t.is(typeof db.getQuestions, 'function', 'getQuestions', 'getQuestions should be a function')

  let questions = fixtures.getQuestions(1)
  let maped = questions.map(l => db.saveQuestion(l))
  let saved = await Promise.all(maped)
  let result = await db.getQuestions()

  t.is(saved.length, result.length)
})

test('get question by id', async t => {
  let db = t.context.db
  t.is(typeof db.getQuestionById, 'function', 'should be a function')

  let q = fixtures.getQuestion()
  let saved = await db.saveQuestion(q)
  let result = await db.getQuestionById(saved.publicId)

  t.deepEqual(saved, result)
})

test('auth user', async t => {
  let db = t.context.db
  t.is(typeof db.authUser, 'function', 'authUser should be a function')

  let user = fixtures.getUser()
  let plainPassword = user.password
  await db.saveUser(user)
  let isAuth = await db.authUser(user.username, plainPassword)

  t.true(isAuth)
})

test('get user', async t => {
  let db = t.context.db
  t.is(typeof db.authUser, 'function', 'getUser should be a function')

  let user = fixtures.getUser()
  await db.saveUser(user)
  let result = await db.getUser(user.username)

  t.is(result.username, user.username)
})

test('save role', async t => {
  let db = t.context.db
  t.is(typeof db.saveRole, 'function', 'saveRol should be a function')

  let rol = fixtures.getRole()
  let saved = await db.saveRole(rol)

  t.is(rol.role, saved.role)
  t.is(rol.active, saved.active)
  t.is(rol.deleted, saved.deleted)
})

test('update role', async t => {
  let db = t.context.db
  t.is(typeof db.updateRole, 'function', 'updateRole should be a function')

  let role = fixtures.getRole()
  let saved = await db.saveRole(role)
  let get = await db.getRoleById(saved.publicId)
  get.active = false

  let updated = await db.updateRole(get)

  t.is(get.active, updated.active)
  t.false(updated.active)
})

test('delete role', async t => {
  let db = t.context.db
  t.is(typeof db.deleteRole, 'function', 'deleteRole should be a function')

  let role = fixtures.getRole()
  let saved = await db.saveRole(role)
  let get = await db.getRoleById(saved.publicId)
  let deleted = await db.deleteRole(get.publicId)

  t.true(deleted.deleted)
})

test('get roles', async t => {
  let db = t.context.db
  t.is(typeof db.getRoles, 'function', 'getRoles should be a function')

  let roles = fixtures.getRoles(1)
  let rolesMap = roles.map(l => db.saveRole(l))
  let saved = await Promise.all(rolesMap)
  let result = await db.getRoles()

  t.is(saved.length, result.length)
})

test('get role by id', async t => {
  let db = t.context.db
  t.is(typeof db.getRoleById, 'function', 'getRoleById should be a function')

  let role = fixtures.getRole()
  let saved = await db.saveRole(role)
  let result = await db.getRoleById(saved.publicId)

  t.deepEqual(saved, result)
})

test('save answer type', async t => {
  let db = t.context.db
  t.is(typeof db.saveAnswerType, 'function', 'save answer type should be a function')

  let answerType = fixtures.getAnswerType()
  let saved = await db.saveAnswerType(answerType)

  t.is(saved.type, answerType.type)
  t.is(saved.active, answerType.active)
  t.is(saved.deleted, answerType.deleted)
})

test('update Answer Type', async t => {
  let db = t.context.db
  t.is(typeof db.updateAnswerType, 'function', 'updateAnswerType should be a function')

  let answer = fixtures.getAnswerType()
  let saved = await db.saveAnswerType(answer)
  let get = await db.getAnswerTypeById(saved.publicId)
  get.active = false

  let updated = await db.updateAnswerType(get)

  t.is(get.active, updated.active)
  t.false(updated.active)
})

test('delete AnswerType', async t => {
  let db = t.context.db
  t.is(typeof db.deleteAnswerType, 'function', 'deleteAnswerType should be a function')

  let answer = fixtures.getAnswerType()
  let saved = await db.saveAnswerType(answer)
  let get = await db.getAnswerTypeById(saved.publicId)
  let deleted = await db.deleteAnswerType(get.publicId)

  t.true(deleted.deleted)
})

test('get answers type', async t => {
  let db = t.context.db
  t.is(typeof db.getAnswersTypes, 'function', 'getAnswersTypes should be a function')

  let answers = fixtures.getAnwersTypes()
  let op = answers.map(l => db.saveAnswerType(l))
  let saved = await Promise.all(op)
  let result = await db.getAnswersTypes()

  t.is(saved.length, result.length)
})

test('get answer type by id', async t => {
  const db = t.context.db
  t.is(typeof db.getAnswerTypeById, 'function', 'getAnswerTypeById should be a function')

  const answerType = fixtures.getAnswerType()
  const saved = await db.saveAnswerType(answerType)
  const result = await db.getAnswerTypeById(saved.publicId)

  t.is(saved.type, result.type)
})
