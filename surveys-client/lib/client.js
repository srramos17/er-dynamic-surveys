'use strict'

const request = require('request-promise')
const Promise = require('bluebird')

class Client {
  constructor (options) {
    this.options = options || {
      endpoints: {
        surveys: 'http://api.cuestionarios.com/survey',
        users: 'http://api.cuestionarios.com/user',
        roles: 'http://api.cuestionarios.com/role',
        groups: 'http://api.cuestionarios.com/group',
        questions: 'http://api.cuestionarios.com/question'
      }
    }
  }

  listSurveys (user, token, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.surveys}/list`,
      body: user,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  getSurveyById (user, token, id, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.surveys}/${id}`,
      body: user,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  createSurvey (survey, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.surveys}/`,
      body: survey,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  updateSurvey (survey, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.surveys}/update`,
      body: survey,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  deleteSurvey (user, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.surveys}/delete`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  listRoles (user, token, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.roles}/list`,
      body: user,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  getRoleById (user, token, id, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.roles}/${id}`,
      body: user,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  createRole (role, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.roles}/`,
      body: role,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  updateRole (role, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.roles}/update`,
      body: role,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  deleteRole (user, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.roles}/delete`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  listGroups (user, token, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.groups}/list`,
      body: user,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  getGroupById (user, token, id, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.groups}/${id}`,
      body: user,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  createGroup (group, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.groups}/`,
      body: group,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  updateGroup (group, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.groups}/update`,
      body: group,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  deleteGroup (user, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.groups}/delete`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  getUsers (user, token, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/list`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  getUserById (user, token, userId, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/${userId}`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  createUser (token, user, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.users}/`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  updateUser (token, user, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.users}/update`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  deleteUser (user, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.users}/delete`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  authUser (user, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.users}/auth`,
      json: true,
      body: user
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  getUser (username, token, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/name/${username}`,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  listQuestions (user, token, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.questions}/list`,
      body: user,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  getQuestionById (user, token, id, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.questions}/${id}`,
      body: user,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  createQuestion (question, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.questions}/`,
      body: question,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  updateQuestion (question, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.questions}/update`,
      body: question,
      json: true,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  deleteQuestion (user, token, callback) {
    const options = {
      method: 'POST',
      uri: `${this.options.endpoints.questions}/delete`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  getDocuments (user, token, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/listDocuments`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  getCompanies (user, token, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/getCompanies`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }

  getBranchesByCompany (user, token, callback) {
    const options = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/getBranchesByCompany`,
      json: true,
      body: user,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }

    return Promise.resolve(request(options)).asCallback(callback)
  }
}

module.exports = Client
