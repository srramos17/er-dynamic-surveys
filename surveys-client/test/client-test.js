'use strict'

const test = require('ava')
const client = require('../')
const fixtures = require('./fixtures')
const nock = require('nock')

const options = {
  endpoints: {
    surveys: 'http://cuestionarios.test/survey',
    users: 'http://cuestionarios.test/user'
  }
}

test.beforeEach(t => {
  t.context.client = client.CreateClient(options)
})

test('cliente', t => {
  const client = t.context.client

  t.is(typeof client.listSurveys, 'function')
  t.is(typeof client.getSurveyById, 'function')
  t.is(typeof client.getSurveyByName, 'function')
  t.is(typeof client.createSurvey, 'function')
  t.is(typeof client.updateSurvey, 'function')
  t.is(typeof client.deleteSurvey, 'function')
  t.is(typeof client.getUsers, 'function')
  t.is(typeof client.getUserById, 'function')
  t.is(typeof client.createUser, 'function')
  t.is(typeof client.updateUser, 'function')
  t.is(typeof client.deleteUser, 'function')
  t.is(typeof client.authUser, 'function')
})

test('listSurvey', async t => {
  const client = t.context.client
  const surveys = fixtures.getSurveys()

  nock(options.endpoints.surveys)
  .get('/list')
  .reply(200, surveys)

  const result = await client.listSurveys()

  t.deepEqual(surveys, result)
})

test('get survey id', async t => {
  const client = t.context.client
  const survey = fixtures.getSurvey()

  nock(options.endpoints.surveys)
  .get(`/${survey.publicId}`)
  .reply(200, survey)

  const result = await client.getSurveyById(survey.publicId)

  t.deepEqual(survey, result)
})

test('get survey name', async t => {
  const client = t.context.client
  const survey = fixtures.getSurvey()

  nock(options.endpoints.surveys)
  .get(`/survey/${survey.name}`)
  .reply(200, survey)

  const result = await client.getSurveyByName(survey.name)

  t.deepEqual(survey, result)
})

test('create survey', async t => {
  const client = t.context.client
  const survey = fixtures.getSurvey()
  const newSurvey = {
    name: survey.name,
    active: survey.active,
    deleted: survey.deleted
  }

  nock(options.endpoints.surveys)
  .post('/', newSurvey)
  .reply(201, survey)

  const result = await client.createSurvey(newSurvey)

  t.deepEqual(result, survey)
})

test('update survey', async t => {
  const client = t.context.client
  const survey = fixtures.getSurvey()
  survey.name = 'Survey updated'

  nock(options.endpoints.surveys)
  .post('/update', survey)
  .reply(200, survey)

  const result = await client.updateSurvey(survey)

  t.deepEqual(survey, result)
})

test('delete survey', async t => {
  const client = t.context.client
  const survey = fixtures.getSurvey()
  const deletedSurvey = {
    name: survey.name,
    active: survey.active,
    deleted: true
  }

  nock(options.endpoints.surveys)
  .delete(`/delete/${survey.publicId}`)
  .reply(200, deletedSurvey)

  const result = await client.deleteSurvey(survey.publicId)

  t.true(result.deleted)
})

test('GetUsers /list', async t => {
  const client = t.context.client
  const userAuth = fixtures.getUserAuth()
  const users = fixtures.getUsers()
  const token = 'xxx-xxx-xxx'

  nock(options.endpoints.users, {
    reqheaders: {
      'Authorization': `Bearer ${token}`
    }
  })
  .get('/list', {userId: userAuth.userId})
  .reply(200, users)

  const result = await client.getUsers(userAuth.userId, token)

  t.deepEqual(users, result)
})

test('GetUserById /:id', async t => {
  const client = t.context.client
  const userAuth = fixtures.getUserAuth()
  const user = fixtures.getUser()
  const token = 'xxx-xxx-xxx'

  nock(options.endpoints.users, {
    reqheaders: {
      'Authorization': `Bearer ${token}`
    }
  })
  .get(`/${user.id}`)
  .reply(200, user)

  const result = await client.getUserById(userAuth.userId, token, user.id)

  t.deepEqual(user, result)
})

test('GetUser /name/:username', async t => {
  const client = t.context.client
  const user = fixtures.getUser()
  const token = 'xxx-xxx-xxx'

  nock(options.endpoints.users, {
    reqheaders: {
      'Authorization': `Bearer ${token}`
    }
  })
  .get(`/name/${user.username}`)
  .reply(200, user)

  const result = await client.getUser(user.username, token)

  t.deepEqual(user, result)
})

test('CreateUser /', async t => {
  const client = t.context.client
  const user = fixtures.getUser()
  const token = 'xxx-xxx-xxx'
  const newUser = {
    lastname: user.lastname,
    username: user.username,
    email: user.email
  }

  nock(options.endpoints.users, {
    reqheaders: {
      'Authorization': `Bearer ${token}`
    }
  })
  .post('/', newUser)
  .reply(201, user)

  const result = await client.createUser(token, newUser)

  t.deepEqual(result, user)
})

test('UpdateUser /update', async t => {
  const client = t.context.client
  const user = fixtures.getUser()
  user.name = 'Anastasio'
  const token = 'xxx-xxx-xxx'

  nock(options.endpoints.users, {
    reqheaders: {
      'Authorization': `Bearer ${token}`
    }
  })
  .post('/update', user)
  .reply(200, user)

  const result = await client.updateUser(token, user)

  t.deepEqual(result, user)
})

test('DeleteUser /delete/:id', async t => {
  const client = t.context.client
  const userAuth = fixtures.getUserAuth()
  const user = fixtures.getUser()
  const deletedUser = {
    lastname: user.lastname,
    username: user.username,
    email: user.email,
    deleted: true
  }
  const token = 'xxx-xxx-xxx'

  nock(options.endpoints.users, {
    reqheaders: {
      'Authorization': `Bearer ${token}`
    }
  })
  .delete(`/delete/${user.id}`, {userId: userAuth.userId})
  .reply(200, deletedUser)

  const result = await client.deleteUser(userAuth.userId, user.id, token)

  t.true(result.deleted)
})

test('AuthUser /authUser', async t => {
  const client = t.context.client
  const authUser = fixtures.getUserAuth()
  const user = {
    username: 'anastasio',
    password: '123456a'
  }

  nock(options.endpoints.users)
  .post('/auth', user)
  .reply(200, authUser)

  const result = await client.authUser(user)

  t.deepEqual(result, authUser)
})
