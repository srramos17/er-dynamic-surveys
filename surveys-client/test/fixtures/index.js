'use strict'

const uuid = require('uuid-base62')

const fixtures = {
  getSurvey () {
    let id = uuid.uuid()
    return {
      id: id,
      publicId: uuid.encode(id),
      name: 'pediatrica',
      active: true,
      deleted: false
    }
  },
  getSurveys () {
    return [
      this.getSurvey(),
      this.getSurvey(),
      this.getSurvey()
    ]
  },
  getUserAuth () {
    return {
      userId: '2qY9COoAhfMrsH7mCyh86T'
    }
  },
  getUser () {
    return {
      id: '2qY9COoAhfMrsH7mCyh86T',
      name: 'Angel',
      lastname: 'Urrutia',
      username: 'aurrutia',
      email: 'email@test.co',
      password: 'foo123',
      rol: '1',
      active: true,
      deleted: false
    }
  },
  getUsers () {
    return [
      this.getUser(),
      this.getUser(),
      this.getUser()
    ]
  }
}

module.exports = fixtures
