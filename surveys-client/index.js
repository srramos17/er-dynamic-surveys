'use strict'

const Client = require('./lib/client')

exports.CreateClient = (options) => {
  return new Client(options)
}
