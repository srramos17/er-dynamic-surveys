'user strict'

const config = {
  db: {
    setup: true
  },
  secret: process.env.SECRET_KEY_CD || 'c.d.1.s3crt'
}

module.exports = config
