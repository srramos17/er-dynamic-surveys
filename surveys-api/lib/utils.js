'use strict'

const jwt = require('jsonwebtoken')
const bearer = require('token-extractor')

const utils = {
  signToken,
  verifyToken,
  extractToken
}

async function signToken (playload, secret, options) {
  return new Promise((resolve, reject) => {
    jwt.sign(playload, secret, options, (err, token) => {
      if (err) return reject(err)

      resolve(token)
    })
  })
}

async function verifyToken (token, secret, options) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, secret, options, (err, decoded) => {
      if (err) return reject(err)

      resolve(decoded)
    })
  })
}

async function extractToken (req) {
  return new Promise((resolve, reject) => {
    bearer(req, (err, token) => {
      if (err) return reject(err)

      resolve(token)
    })
  })
}

module.exports = utils
