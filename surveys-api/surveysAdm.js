'use strict'

const HttpHash = require('http-hash')
const {send, json} = require('micro')
const Db = require('surveys-db')
const config = require('./config')
const utils = require('./lib/utils')
const DbSutb = require('./test/stub/db')

const env = process.env.NODE_ENV || 'production'

let db = new Db(config.db)

if (env === 'dev') {
  db = new DbSutb()
}

const hash = HttpHash()

hash.set('GET /list', async function listSurveys (req, res, params) {
  const user = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)

    if (decode && decode.userId !== user.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let surveys = await db.getSurveys(user)
  await db.disconnect()

  send(res, 200, surveys)
})

hash.set('GET /:id', async function getSurveyById (req, res, params) {
  let id = params.id
  const user = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let survey = await db.getSurveyById(id)
  await db.disconnect()

  send(res, 200, survey)
})

hash.set('POST /', async function postSurvey (req, res, params) {
  const survey = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== survey.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  const result = await db.saveSurvey(survey)
  await db.disconnect()

  send(res, 201, result)
})

hash.set('POST /update', async function updateSurvey (req, res, params) {
  let survey = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== survey.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  const result = await db.updateSurvey(survey)
  await db.disconnect()

  send(res, 200, result)
})

hash.set('POST /delete', async function deleteSurvey (req, res, params) {
  const user = await json(req)

  try {
    const token = await utils.extractToken(req)
    const decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    send(res, 401, {error: e.message})
  }

  await db.connect()
  let result = null
  let polles = user.polles

  await Promise.all(polles.map(async (poll) => {
    result = await db.deleteSurvey(poll)
  }))

  await db.disconnect()

  send(res, 200, result)
})

async function main (req, res) {
  let { method, url } = req
  let match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, {error: e.message})
    }
  } else {
    send(res, 404, {error: 'route no found'})
  }
}

module.exports = main
