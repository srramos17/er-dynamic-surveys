'user strict'

const fixtures = require('../fixtures')

class Db {
  connect () {
    return Promise.resolve(true)
  }
  disconnect () {
    return Promise.resolve(true)
  }
  getSurveyById (id) {
    return Promise.resolve(fixtures.getSurveyAdm())
  }
  getSurveys () {
    return Promise.resolve(fixtures.getSurveysAdm())
  }
  saveSurvey () {
    return Promise.resolve(fixtures.getSurveyAdm())
  }
  getSurveyByName () {
    return Promise.resolve(fixtures.getSurveyAdm())
  }
  updateSurvey (a) {
    let survey = fixtures.getSurveyAdm()
    survey.name = 'Survey Updated'
    return Promise.resolve(survey)
  }
  deleteSurvey (id) {
    let survey = fixtures.getSurveyAdm()
    survey.deleted = true
    return Promise.resolve(survey)
  }
  getUsers () {
    return Promise.resolve(fixtures.getUsers())
  }
  getUser () {
    return Promise.resolve(fixtures.getUser())
  }
  getUserById () {
    return Promise.resolve(fixtures.getUser())
  }
  saveUser () {
    return Promise.resolve(fixtures.getUser())
  }
  updateUser () {
    let user = fixtures.getUser()
    user.name = 'User updated'
    return Promise.resolve(user)
  }
  deleteUser () {
    let user = fixtures.getUser()
    user.deleted = true
    return Promise.resolve(user)
  }
  authUser (username, password) {
    return Promise.resolve(true)
  }
}

module.exports = Db
