'use strict'

const test = require('ava')
const micro = require('micro')
const listen = require('test-listen')
const request = require('request-promise')
const users = require('../users')
const fixtures = require('./fixtures')
const utils = require('../lib/utils')
const config = require('../config')

test.beforeEach(async t => {
  let server = micro(users)
  t.context.url = await listen(server)
})

test('GetUsers GET /list', async t => {
  const url = t.context.url
  const users = fixtures.getUsers()
  const userAuth = fixtures.getUserAuth()
  const token = await utils.signToken({userId: userAuth.userId}, config.secret)
  const options = {
    method: 'GET',
    uri: `${url}/list`,
    json: true,
    body: {
      userId: userAuth.userId
    },
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  const body = await request(options)
  t.deepEqual(users, body)
})

test('Get UserById GET /:id', async t => {
  const url = t.context.url
  const user = fixtures.getUser()
  const userAuth = fixtures.getUserAuth()
  const token = await utils.signToken({userId: userAuth.userId}, config.secret)
  const options = {
    method: 'GET',
    uri: `${url}/${user.id}`,
    json: true,
    body: {
      userId: userAuth.userId
    },
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  const body = await request(options)
  t.deepEqual(user, body)
})

test('Create User POST /', async t => {
  const url = t.context.url
  const user = fixtures.getUser()
  const userAuth = fixtures.getUserAuth()
  const token = await utils.signToken({userId: userAuth.userId}, config.secret)
  const options = {
    method: 'POST',
    uri: url,
    json: true,
    body: {
      name: user.name,
      lastname: user.lastname,
      username: user.username,
      email: user.email,
      password: user.password,
      rol: user.rol,
      active: user.active,
      deleted: user.deleted,
      userId: userAuth.userId
    },
    headers: {
      'Authorization': `Bearer ${token}`
    },
    resolveWithFullResponse: true
  }

  const response = await request(options)

  t.is(response.statusCode, 201)
  t.deepEqual(user, response.body)
})

test('Update User POST /update', async t => {
  const url = t.context.url
  const user = fixtures.getUser()
  const userAuth = fixtures.getUserAuth()
  const token = await utils.signToken({userId: userAuth.userId}, config.secret)
  const options = {
    method: 'POST',
    uri: `${url}/update`,
    json: true,
    body: {
      name: user.name,
      lastname: user.lastname,
      username: user.username,
      email: user.email,
      password: user.password,
      rol: user.rol,
      active: user.active,
      deleted: user.deleted,
      userId: userAuth.userId
    },
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  const response = await request(options)
  const userUpdated = JSON.parse(JSON.stringify(user))
  userUpdated.name = 'User updated'

  t.deepEqual(response, userUpdated)
})

test('Delete User DELETE /delete/:id', async t => {
  const url = t.context.url
  const user = fixtures.getUser()
  const userAuth = fixtures.getUserAuth()
  const token = await utils.signToken({userId: userAuth.userId}, config.secret)
  const options = {
    method: 'DELETE',
    uri: `${url}/delete/${user.id}`,
    json: true,
    body: {
      userId: userAuth.userId
    },
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  const response = await request(options)
  t.true(response.deleted)
})

test('Auth User POST /AuthUser', async t => {
  const url = t.context.url
  const user = fixtures.getUser()
  const options = {
    method: 'POST',
    uri: `${url}/auth`,
    json: true,
    body: {
      username: user.username,
      password: user.password
    }
  }

  const token = await request(options)
  const decoded = await utils.verifyToken(token, config.secret)

  t.is(decoded.username, user.username)
})

test('Get User GET /GetUser', async t => {
  const url = t.context.url
  const user = fixtures.getUser()
  const token = await utils.signToken({userId: user.id}, config.secret)
  const options = {
    method: 'GET',
    uri: `${url}/name/${user.username}`,
    json: true,
    body: {
      userId: user.id
    },
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  const body = await request(options)
  t.deepEqual(user, body)
})
