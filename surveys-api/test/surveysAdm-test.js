'use strict'

const test = require('ava')
const micro = require('micro')
const listen = require('test-listen')
const request = require('request-promise')
const surveys = require('../surveysAdm')
const fixtures = require('./fixtures')
const utils = require('../lib/utils')
const config = require('../config')

test.beforeEach(async t => {
  const server = micro(surveys)
  t.context.url = await listen(server)
})

test('Survey GET /list', async t => {
  const url = t.context.url
  const surveys = fixtures.getSurveysAdm()
  const user = fixtures.getUserAuth()
  const token = await utils.signToken({userId: user.userId}, config.secret)
  const options = {
    method: 'GET',
    uri: `${url}/list`,
    json: true,
    body: {
      userId: user.userId
    },
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  const body = await request(options)

  t.deepEqual(surveys, body)
})

test('Survey GET /:id', async t => {
  const url = t.context.url
  const survey = fixtures.getSurveyAdm()
  const user = fixtures.getUserAuth()
  const token = await utils.signToken({userId: user.userId}, config.secret)
  const options = {
    method: 'GET',
    uri: `${url}/${url.id}`,
    json: true,
    body: {
      userId: user.userId
    },
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  const body = await request(options)

  t.deepEqual(survey, body)
})

test('Survey GET /survey/:surveyname', async t => {
  const url = t.context.url
  const survey = fixtures.getSurveyAdm()
  const user = fixtures.getUserAuth()
  const token = await utils.signToken({userId: user.userId}, config.secret)
  const options = {
    method: 'GET',
    uri: `${url}/survey/${survey.name}`,
    json: true,
    body: {
      userId: user.userId
    },
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  const body = await request(options)

  t.deepEqual(survey, body)
})

test('Survey POST /', async t => {
  const url = t.context.url
  const survey = fixtures.getSurveyAdm()
  const user = fixtures.getUserAuth()
  const token = await utils.signToken({userId: user.userId}, config.secret)
  const options = {
    method: 'POST',
    uri: url,
    json: true,
    body: {
      id: survey.id,
      name: survey.name,
      active: survey.active,
      delete: survey.delete,
      userId: user.userId
    },
    headers: {
      'Authorization': `Bearer ${token}`
    },
    resolveWithFullResponse: true
  }

  const response = await request(options)

  t.is(response.statusCode, 201)
  t.deepEqual(survey, response.body)
})

test('Survey POST /update', async t => {
  const url = t.context.url
  const survey = fixtures.getSurveyAdm()
  const user = fixtures.getUserAuth()
  const token = await utils.signToken({userId: user.userId}, config.secret)
  const options = {
    method: 'POST',
    uri: `${url}/update`,
    json: true,
    body: {
      id: survey.id,
      name: survey.name,
      active: survey.active,
      delete: survey.delete,
      userId: user.userId
    },
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  const response = await request(options)

  let newSurvey = JSON.parse(JSON.stringify(survey))
  newSurvey.name = 'Survey Updated'

  t.deepEqual(response, newSurvey)
})

test('DELETE /delete/:id', async t => {
  const url = t.context.url
  const survey = fixtures.getSurveyAdm()
  const user = fixtures.getUserAuth()
  const token = await utils.signToken({userId: user.userId}, config.secret)
  const options = {
    method: 'DELETE',
    uri: `${url}/delete/${survey.id}`,
    json: true,
    body: {
      userId: user.userId
    },
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  const response = await request(options)

  t.true(response.deleted)
})
