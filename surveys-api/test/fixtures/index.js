'use strict'

const fixtures = {
  getSurveyAdm () {
    return {
      id: '2qY9COoAhfMrsH7mCyh86T',
      name: 'UCI-pediatrica',
      active: true,
      deleted: false
    }
  },
  getSurveysAdm () {
    return [
      this.getSurveyAdm(),
      this.getSurveyAdm(),
      this.getSurveyAdm()
    ]
  },
  getUserAuth () {
    return {
      userId: '2qY9COoAhfMrsH7mCyh86T'
    }
  },
  getUser () {
    return {
      id: '2qY9COoAhfMrsH7mCyh86T',
      name: 'Angel',
      lastname: 'Urrutia',
      username: 'aurrutia',
      email: 'email@test.co',
      password: 'foo123',
      rol: '1',
      active: true,
      deleted: false
    }
  },
  getUsers () {
    return [
      this.getUser(),
      this.getUser(),
      this.getUser()
    ]
  }
}

module.exports = fixtures
