'use strict'

const test = require('ava')
const micro = require('micro')
const listen = require('test-listen')
// const request = require('request-promise')
const surveys = require('../surveysAdm')
// const fixtures = require('./fixtures')
// const utils = require('../lib/utils')
// const config = require('../config')

test.beforeEach(async t => {
  let server = micro(surveys)
  t.context.url = await listen(server)
})

test.todo('GET /list')
test.todo('GET /:id')
test.todo('GET /:surveyName')
test.todo('POST /')
test.todo('POST /update')
