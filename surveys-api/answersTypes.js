'use strict'

const HttpHash = require('http-hash')
const {send} = require('micro')
const Db = require('surveys-db')
const config = require('./config')
// const utils = require('./lib/utils')
const DbSutb = require('./test/stub/db')

const env = process.env.NODE_ENV || 'production'

let db = new Db(config.db)

if (env === 'dev') {
  db = new DbSutb()
}

const hash = HttpHash()

hash.set('GET /list', async function listSurveys (req, res, params) {
  await db.connect()
  let surveys = await db.getSurveysAdm()
  await db.disconnect()

  send(res, 200, surveys)
})

async function main (req, res) {
  let { method, url } = req
  let match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, {error: e.message})
    }
  } else {
    send(res, 404, {error: 'route no found'})
  }
}

module.exports = main
