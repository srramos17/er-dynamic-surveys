'use strict'

const HttpHash = require('http-hash')
const {send, json} = require('micro')
const Db = require('surveys-db')
const config = require('./config')
const utils = require('./lib/utils')
const DbSutb = require('./test/stub/db')

const env = process.env.NODE_ENV || 'production'

let db = new Db(config.db)

if (env === 'dev') {
  db = new DbSutb()
}

const hash = HttpHash()

hash.set('GET /list', async function listRoles (req, res, params) {
  const user = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)

    if (decode && decode.userId !== user.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let roles = await db.getRoles(user)
  await db.disconnect()

  send(res, 200, roles)
})

hash.set('GET /:id', async function getRoleById (req, res, params) {
  let id = params.id
  const user = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let role = await db.getRoleById(id)
  await db.disconnect()

  send(res, 200, role)
})

hash.set('POST /', async function postRole (req, res, params) {
  const role = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== role.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  delete role.userId

  await db.connect()
  const result = await db.saveRole(role)
  await db.disconnect()

  send(res, 201, result)
})

hash.set('POST /update', async function updateRole (req, res, params) {
  let role = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== role.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  const result = await db.updateRole(role)
  await db.disconnect()

  send(res, 200, result)
})

hash.set('POST /delete', async function deleteRole (req, res, params) {
  const user = await json(req)

  try {
    const token = await utils.extractToken(req)
    const decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    send(res, 401, {error: e.message})
  }

  await db.connect()
  let result = null
  let roles = user.roles

  await Promise.all(roles.map(async (rol) => {
    result = await db.deleteRole(rol)
  }))

  await db.disconnect()

  send(res, 200, result)
})

async function main (req, res) {
  let { method, url } = req
  let match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, {error: e.message})
    }
  } else {
    send(res, 404, {error: 'route no found'})
  }
}

module.exports = main
