'use strict'

const HttpHash = require('http-hash')
const {send, json} = require('micro')
const Db = require('surveys-db')
const config = require('./config')
const utils = require('./lib/utils')
const DbSutb = require('./test/stub/db')

const env = process.env.NODE_ENV || 'production'

let db = new Db(config.db)

if (env === 'dev') {
  db = new DbSutb()
}

const hash = HttpHash()

hash.set('GET /list', async function listGroups (req, res, params) {
  const user = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)

    if (decode && decode.userId !== user.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let groups = await db.getGroups(user)
  await db.disconnect()

  send(res, 200, groups)
})

hash.set('GET /:id', async function getGroupById (req, res, params) {
  let id = params.id
  const user = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let group = await db.getGroupById(id)
  await db.disconnect()

  send(res, 200, group)
})

hash.set('POST /', async function postGroup (req, res, params) {
  const group = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== group.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  delete group.userId

  await db.connect()
  const result = await db.saveGroup(group)
  await db.disconnect()

  send(res, 201, result)
})

hash.set('POST /update', async function updateGroup (req, res, params) {
  let group = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== group.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  const result = await db.updateGroup(group)
  await db.disconnect()

  send(res, 200, result)
})

hash.set('POST /delete', async function deleteGroup (req, res, params) {
  const user = await json(req)

  try {
    const token = await utils.extractToken(req)
    const decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('invalid token')
    }
  } catch (e) {
    send(res, 401, {error: e.message})
  }

  await db.connect()
  let result = null
  let groups = user.groups

  await Promise.all(groups.map(async (id) => {
    result = await db.deleteGroup(id)
  }))

  await db.disconnect()

  send(res, 200, result)
})

async function main (req, res) {
  let { method, url } = req
  let match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, {error: e.message})
    }
  } else {
    send(res, 404, {error: 'route no found'})
  }
}

module.exports = main
