'use strict'

const HttpHash = require('http-hash')
const {send, json} = require('micro')
const Db = require('surveys-db')
const config = require('./config')
const utils = require('./lib/utils')
const DbSutb = require('./test/stub/db')

const env = process.env.NODE_ENV || 'production'

let db = new Db(config.db)

if (env === 'dev') {
  db = new DbSutb()
}

const hash = HttpHash()

hash.set('GET /list', async function listUsers (req, res, params) {
  const user = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('Invalid Token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let users = await db.getUsers(user)
  await db.disconnect()

  send(res, 200, users)
})

hash.set('GET /:id', async function getUserById (req, res, params) {
  const user = await json(req)
  const idUser = params.id

  try {
    const token = await utils.extractToken(req)
    const decode = await utils.verifyToken(token, config.secret)

    if (decode && decode.userId !== user.userId) {
      throw new Error('Invalid Token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let userFound = await db.getUserById(idUser)
  await db.disconnect()

  delete userFound.password

  send(res, 200, userFound)
})

hash.set('POST /', async function CreateUser (req, res, params) {
  const user = await json(req)

  try {
    const token = await utils.extractToken(req)
    const decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('Invalid Token')
    }
  } catch (e) {
    send(res, 401, {error: e.message})
  }

  await db.connect()
  const userCreated = await db.saveUser(user)
  await db.disconnect()

  send(res, 201, userCreated)
})

hash.set('POST /update', async function UpdateUser (req, res, params) {
  const user = await json(req)

  try {
    const token = await utils.extractToken(req)
    const decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('Invalid Token')
    }
  } catch (e) {
    send(res, 401, {error: e.message})
  }

  await db.connect()
  const result = await db.updateUser(user)
  await db.disconnect()

  send(res, 201, result)
})

hash.set('POST /delete', async function DeleteUser (req, res, params) {
  const user = await json(req)

  try {
    const token = await utils.extractToken(req)
    const decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('Invalid Token')
    }
  } catch (e) {
    send(res, 401, {error: e.message})
  }

  await db.connect()
  let result = null
  let users = user.users

  await Promise.all(users.map(async (usr) => {
    result = await db.deleteUser(usr)
  }))

  await db.disconnect()

  send(res, 201, result)
})

hash.set('POST /auth', async function AuthUser (req, res, params) {
  const user = await json(req)

  await db.connect()
  let result = await db.authUser(user.username, user.password)
  await db.disconnect()

  if (!result) {
    return send(res, 401, {error: 'Invalid Credentials'})
  }

  await db.connect()
  let userFound = await db.getUser(user.username)
  await db.disconnect()

  delete userFound.password
  delete userFound.email

  const token = await utils.signToken({username: user.username, userId: userFound.userId}, config.secret)

  send(res, 201, token)
})

hash.set('GET /name/:username', async function getUser (req, res, params) {
  const username = params.username

  try {
    const token = await utils.extractToken(req)
    const decode = await utils.verifyToken(token, config.secret)

    if (decode && !decode.userId) {
      throw new Error('Invalid Token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let userFound = await db.getUser(username)
  await db.disconnect()

  delete userFound.password
  send(res, 200, userFound)
})

hash.set('GET /listDocuments', async function listDocuments (req, res, params) {
  const user = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('Invalid Token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let documents = await db.getTypeDocuments()
  await db.disconnect()

  send(res, 200, documents)
})

hash.set('GET /getCompanies', async function getCompanies (req, res, params) {
  const user = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('Invalid Token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let documents = await db.getCompanies(user)
  await db.disconnect()

  send(res, 200, documents)
})

hash.set('GET /getBranchesByCompany', async function getBranchesByCompany (req, res, params) {
  const user = await json(req)

  try {
    let token = await utils.extractToken(req)
    let decode = await utils.verifyToken(token, config.secret)
    if (decode && decode.userId !== user.userId) {
      throw new Error('Invalid Token')
    }
  } catch (e) {
    return send(res, 401, {error: e.message})
  }

  await db.connect()
  let documents = await db.getBranchesByCompany(user)
  await db.disconnect()

  send(res, 200, documents)
})

async function main (req, res) {
  let { method, url } = req
  let match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, {error: e.message})
    }
  } else {
    send(res, 404, {error: 'route no found'})
  }
}

module.exports = main
